public class SequenceStatic {
	private static int currentValue = 0;

	private SequenceStatic() { // Deny instantiation of this class
	}

	public static int nextValue() {
		currentValue++;
		return currentValue;
	}
}