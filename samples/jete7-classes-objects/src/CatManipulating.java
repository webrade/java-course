import java.util.Scanner;

public class CatManipulating {
	
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Write first cat name: ");
		String firstCatName = input.nextLine();
		// Assign cat name with a constructor
		Cat firstCat = new Cat(firstCatName);

		System.out.println("Write second cat name: ");
		Cat secondCat = new Cat();
		// Assign cat name with a property
		secondCat.setName(input.nextLine());

		// Create a cat with a default name
		Cat thirdCat = new Cat();
		Cat[] cats = new Cat[] { firstCat, secondCat, thirdCat };

		for (Cat cat : cats) {
			cat.SayMiau();
		}
	}
}
