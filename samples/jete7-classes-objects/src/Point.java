public class Point {
	// Definition of fields (member variables)
	private int xCoord;

	private int yCoord;

	private String name;

	// The default constructor calls the other constructor
	public Point() {
		this(0, 0);

	}

	// Constructor to parameters
	public Point(int xCoord, int yCoord) {
		this.xCoord = xCoord;
		this.yCoord = yCoord;

		name = String.format("(%d,%d)", this.xCoord, this.yCoord);
	}

	// Getter And Setter of the Property XCoord
	public int getXCoord() {
		return this.xCoord;
	}

	public void setXCoord(int value) {
		this.xCoord = value;
	}

	// Getter And Setter of the Property YCoord
	public int getYCoord() {
		return this.yCoord;
	}

	public void setYCoord(int value) {
		this.yCoord = value;
	}

	// Getter And Setter of the Property Name
	public String getName() {
		return this.name;
	}

	public void setName(String value) {
		this.name = value;
	}

}
