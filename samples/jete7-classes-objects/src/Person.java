public class Person {
	private String name;

	private int age;

	// Default constructor
	public Person() {
		this.name = null;
		this.age = 0;
	}

	// Constructor with parameters
	public Person(String name, int age) {
		this.name = name;
		this.age = age;
	}

	// getter and setter of the property "name"
	public String getName() {
		return this.name;
	}

	public void setName(String value) {
		this.name = value;
	}

	// getter and setter of the property "age"
	public int getAge() {
		return this.age;
	}

	public void setAge(int value) {
		this.age = value;
	}
}
