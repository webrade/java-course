package demo;

import courseManagement.*;
import university.*;

public class PackagesDemo {

	public static void main(String[] args) {
		Professor prof = new Professor("Svetlin Nakov");
		Student stud1 = new Student("Pesho Peshev");
		Student stud2 = new Student("Kiro Kirov");
		Student stud3 = new Student("Kaka Mara");
		Student[] students = new Student[] { stud1, stud2, stud3 };

		Course course = new Course("Introduction to Java", prof, students);

		System.out.println("Course: " + course.getName());
		System.out.println("Professor: " + course.getProfessor());
		System.out.println("Students:");
		for (Student stud : course.getStudents()) {
			System.out.println("  " + stud);
		}
	}

}
