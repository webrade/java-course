<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Edit Product - ${product.id}</title>
</head>
<body>

<h1>Edit Product</h1>
<hr/>

<form method="POST" action="products">

	<label>Name:</label> 
	<input type="text" name="name" value="${product.name}" /> <br/>
	
	<label>Price:</label> 
	<input type="number" name="price" value="${product.price}" /> <br/>
	
	<label>Description:</label> 
	<textarea name="description">${product.description}</textarea> <br/><br/>

	<input type="hidden" name="edit" value="1" />
	<input type="hidden" name="productId" value="${product.id}" />
	
	<input type="submit" value="Edit Product" />
	
</form>

</body>
</html>