<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>List Products</title>
</head>
<body>

	<h1>Products</h1>
	
	<c:if test="${!(empty newProductMessage)}">
		<p style="color:green"> New Product Successfully Added</p>
	</c:if>
	
	<c:if test="${!(empty editProductMessage)}">
		<p style="color:blue"> Product Successfully Edited</p>
	</c:if>
		
	<hr />
	<p>
		<a href="/samplesql/products?add=1">Add Another Product</a>
	</p>
	<c:forEach items="${products}" var="product">
		<p>
			<small>${product.id}, ${product.name}, ${product.description}, ${product.price}</small>
			</br> <a href="/samplesql/products?edit=${product.id}">Edit This
				Product</a>
		</p>
		<hr />
	</c:forEach>

</body>
</html>