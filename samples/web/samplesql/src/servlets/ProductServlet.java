package servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import domains.Product;

/**
 * Servlet implementation class ProductServlet
 */
public class ProductServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	DataSource dataSource;

	public void init(ServletConfig config) throws ServletException {
		ServletContext sContext = config.getServletContext();

		InitialContext context;
		try {
			context = new InitialContext();
			dataSource = (DataSource) context.lookup("java:comp/env/" + sContext.getInitParameter("poolName"));
			if (dataSource == null)
				throw new ServletException("Unknown DataSource '" + sContext.getInitParameter("poolName") + "'");
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		String addParameter = req.getParameter("add");
		String editParameter = req.getParameter("edit");
		req.setAttribute("newProductMessage", req.getParameter("newProduct"));
		req.setAttribute("editProductMessage", req.getParameter("editProduct"));
		
		String viewToShow = "WEB-INF/products/list.jsp";

		// show add product view
		if (addParameter != null) {
			viewToShow = "WEB-INF/products/add.jsp";
			req.getRequestDispatcher(viewToShow).forward(req, resp);
			return;
		}

		if (editParameter != null) {
			
			Product product = null;
			int productId = Integer.valueOf(editParameter);
			try {
				Connection c = dataSource.getConnection();
				ResultSet rs = c.createStatement().executeQuery("SELECT * from products WHERE id=" + productId);
				
				if(rs.next()) {
					product = new Product();
					product.setId(rs.getInt("id"));
					product.setName(rs.getString("name"));
					product.setPrice(rs.getDouble("price"));
					product.setDescription(rs.getString("description"));
				}
				
				req.setAttribute("product", product);
				viewToShow = "WEB-INF/products/edit.jsp";
				req.getRequestDispatcher(viewToShow).forward(req, resp);
				c.close();
				return;
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
		}

		// no add or edit logic, so show all products
		
		List<Product> products = new ArrayList<>();
		req.setAttribute("products", products);

		try {
			Connection c = dataSource.getConnection();
			ResultSet allProducts = c.createStatement().executeQuery(
					"select * from products p");

			while (allProducts.next()) {
				Product p = new Product();
				p.setDescription(allProducts.getString("description"));
				p.setId(allProducts.getInt("id"));
				p.setName(allProducts.getString("name"));
				p.setPrice(allProducts.getDouble("price"));
				products.add(p);
			}

			c.close();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		
		req.getRequestDispatcher(viewToShow).forward(req, resp);
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		String addParam = req.getParameter("add");
		String editParam = req.getParameter("edit");
		
		// for edit 
		String productId = req.getParameter("productId");
		Double price = Double.valueOf(req.getParameter("price"));
		String description = req.getParameter("description");
		String name = req.getParameter("name");
		
		String redirectView = "products";
		boolean successfulChange = false;
		
		if(addParam != null) {
			// add the product
			try {
				Connection c = dataSource.getConnection();
				String insertQuery = "INSERT INTO products (name,price,description) VALUES('%s',%s,'%s')";
				insertQuery = String.format(insertQuery, name, price, description);
				c.createStatement().executeUpdate(insertQuery);
				successfulChange = true;
				c.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			if(successfulChange) {
				redirectView += "?newProduct=true";
			}
			resp.sendRedirect(redirectView);
			return;
		}
		
		if(editParam != null && productId != null) {

			try {
				Connection c = dataSource.getConnection();
				String editQuery = "UPDATE products SET name='%s', price=%s, description='%s' WHERE id=%d";
				editQuery = String.format(editQuery, name, price, description, Integer.parseInt(productId));
				c.createStatement().executeUpdate(editQuery);
				successfulChange = true;
				c.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			if(successfulChange) {
				redirectView += "?editProduct=true";
			}
			resp.sendRedirect(redirectView);
		}
		
	}
}
