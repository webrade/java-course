package model;

public class Department extends Entity {

	private int id;
	private String name;
	private Direction direction;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Direction getDirection() {
		return direction;
	}

	public void setDirection(Direction direction) {
		this.direction = direction;
	}

	@Override
	public String toString() {
		return "Department [id=" + id + ", name=" + name + ", direction=" + direction + "]";
	}

	
	
}
