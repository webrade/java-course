package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import services.ConnectionService;

public abstract class AbstractDAO<T> {

	private Connection connection;

	public AbstractDAO() {
		try {
			this.connection = ConnectionService.getConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public AbstractDAO(Connection conn) {
		this.connection = conn;
	}

	public List<T> findAll() {
		return null;
	}

	public T find(String id) {
		return null;
	};

	public abstract void remove(String id);

	protected ResultSet executeQuery(String query) {
		try {
			return this.connection.createStatement().executeQuery(query);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public void close() {
		try {
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();

		}
	}

	public Connection getConnection() {
		return this.connection;
	}

}
