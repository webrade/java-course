<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:import url="/WEB-INF/include/header.jsp"></c:import>
<section id="intro" class="intro-section">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<p>
					<a class="btn btn-default" href="${appPath}">Обратно към
						началната страница</a>
				</p>
				<hr />
				<p>
					Вие разглеждате <strong>домайн обект</strong>
				</p>
				<h1>
					<code> >> ${currentType} << </code>
				</h1>
				<hr />
				<c:if test="${removed}">
					<div class="alert alert-danger">Успешно изтриване на запис</div>
				</c:if>
				<c:if test="${added}">
					<div class="alert alert-success">Успешно добавяне на запис</div>
				</c:if>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<h1>
					<code>Всички ${currentType}</code>
				</h1>
				<hr />
				<c:import url="/WEB-INF/pages/${currentType}.jsp"></c:import>
				<h1>
					<code>Добави нов обект</code>
				</h1>
				<hr />
				<c:import url="/WEB-INF/pages/add/${currentType}.jsp"></c:import>
				<hr />
			</div>
		</div>
	</div>
</section>
<section>
	<div class="container">
		<c:import url="/WEB-INF/include/boxes.jsp"></c:import>
	</div>
</section>

<c:import url="/WEB-INF/include/footer.jsp"></c:import>