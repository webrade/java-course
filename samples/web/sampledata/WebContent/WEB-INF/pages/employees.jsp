<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<table class="table table-striped">
	<thead>
		<tr>
			<th>Идентификатор</th>
			<th>Име</th>
			<th>Заплата</th>
			<th>Отдел</th>
			<th>#</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${entries}" var="employee">
			<tr>
				<td>${employee.id}</td>
				<td>${employee.name}</td>
				<td><fmt:formatNumber value="${employee.salary}" type="currency" /></td>
				<td>${employee.department.name}</td>
				<td><a href="${appPath}app/employees?remove=${employee.id}" class="btn btn-danger btn-xs">Remove</a></td>
			</tr>
		</c:forEach>
	</tbody>
</table>