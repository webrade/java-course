<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<table class="table table-striped">
	<thead>
		<tr>
			<th>Идентификатор</th>
			<th>Име</th>
			<th>#</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${entries}" var="direction">
			<tr>
				<td>${direction.id}</td>
				<td>${direction.name}</td>
				<td><a href="${appPath}app/directions?remove=${direction.id}" class="btn btn-danger btn-xs">Remove</a></td>
			</tr>
		</c:forEach>
	</tbody>
</table>