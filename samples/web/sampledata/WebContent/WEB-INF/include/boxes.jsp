<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div class="row">

	<div class="col-lg-4">
		<div class="panel panel-default">
			<div class="panel-heading">Дирекции</div>
			<div class="panel-body">
				<p>Управление на дирекции</p>
				<a class="btn btn-md btn-default" href="${appPath}app/directions">
					Избери </a>
			</div>
		</div>
	</div>

	<div class="col-lg-4">
		<div class="panel panel-default">
			<div class="panel-heading">Отдели</div>
			<div class="panel-body">
				<p>Управление на отдели</p>
				<a class="btn btn-md btn-default" href="${appPath}app/departments">
					Избери </a>
			</div>
		</div>
	</div>

	<div class="col-lg-4">
		<div class="panel panel-default">
			<div class="panel-heading">Работници</div>
			<div class="panel-body">
				<p>Управление на работници</p>
				<a class="btn btn-md btn-default" href="${appPath}app/employees">
					Избери </a>
			</div>
		</div>
	</div>

</div>