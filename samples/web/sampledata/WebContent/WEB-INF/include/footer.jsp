		<footer class="footer">
			<div class="container">
				<p>Copyright &copy; 2016 <small>My company</small></p>
			</div>
		</footer>
		<script src="${appPath}static/js/jquery.js"></script>
		<script src="${appPath}static/js/bootstrap.min.js"></script>
		<script src="${appPath}static/js/jquery.easing.min.js"></script>
		<script src="${appPath}static/js/scrolling-nav.js"></script>
	</body>
</html>