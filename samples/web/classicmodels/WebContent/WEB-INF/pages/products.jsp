<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="format" %>
<%@ taglib tagdir="/WEB-INF/tags/template" prefix="template" %>
<%@ taglib tagdir="/WEB-INF/tags/common" prefix="common" %>


<template:template>
	<div class="container">
		
		<common:resultsheading type="Products" />
		
		<table>
			<thead>
				<tR>
					<th>Code</th>
					<th>Name</th>
					<th>Quantity</th>
					<th>Buy Price</th>
					<th>Vendor</th>
					<th>Scale</th>
					<th>View</th>
				</tR>
			</thead>
				
			<tbody>
				<c:forEach items="${results}" var="product">
					<tr>
						<td>${product.productCode}</td>
						<td>${product.productName}</td>
						<td>${product.quantity}</td>
						<td><format:formatNumber value="${product.buyPrice}" type="currency" /></td>
						<td>${product.vendor}</td>
						<td>${product.scale}</td>
						<td><a href="${appPath}products?uid=${product.productCode}">View</a></td>
					</tr>		
				</c:forEach>
			</tbody>
		</table>
	</div>
</template:template>