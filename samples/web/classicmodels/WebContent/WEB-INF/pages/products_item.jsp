<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="format" %>
<%@ taglib tagdir="/WEB-INF/tags/template" prefix="template" %>
<%@ taglib tagdir="/WEB-INF/tags/common" prefix="common" %>

<template:template>
	<div class="container">
		
		<common:singleitemheading type="Product" uid="${uid}" returnUri="products" />
		
		<table>
			<thead>
				<tR>
					<th>Code</th>
					<th>Name</th>
					<th>Quantity</th>
					<th>Buy Price</th>
					<th>Vendor</th>
					<th>Scale</th>
				</tR>
			</thead>

			<tbody>
					<tr>
						<td>${result.productCode}</td>
						<td>${result.productName}</td>
						<td>${result.quantity}</td>
						<td><format:formatNumber value="${result.buyPrice}" type="currency" /></td>
						<td>${result.vendor}</td>
						<td>${result.scale}</td>
					</tr>		
			</tbody>
		</table>
	</div>
</template:template>