<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="format" %>
<%@ taglib tagdir="/WEB-INF/tags/template" prefix="template" %>
<%@ taglib tagdir="/WEB-INF/tags/common" prefix="common" %>

<template:template>
	<div class="container">
		
		<common:resultsheading type="Orders" />
		
		<table>
			<thead>
				<tR>
					<th>Number</th>
					<th>Order Date</th>
					<th>Shipped Date</th>
					<th>Status</th>
					<th>Comments</th>
					<th>View</th>
				</tR>
			</thead>

			<tbody>
				<c:forEach items="${results}" var="order">
					<tr>
						<td>${order.code}</td>
						<td>
							<format:formatDate value="${order.orderDate}" pattern="dd/MM/YYYY" />
						</td>
						<td>
							<format:formatDate value="${order.shippedDate}" pattern="dd/MM/YYYY" />
						</td>
						<td>${order.status}</td>
						<td>${order.comments}</td>
						<td><a href="${appPath}orders?uid=${order.code}">View</a></td>
					</tr>		
				</c:forEach>
			</tbody>
		</table>
	</div>
</template:template>