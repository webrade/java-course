<%@ tag body-content="scriptless" language="java" pageEncoding="ISO-8859-1"%>
<%@attribute name="type" type="java.lang.String" required="true" %> 

<h1> --- All ${type} --- </h1>
<p><a href="${appPath}">Back to Home</a></p>
<hr />