package main.converters;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import main.domain.Order;
import main.domain.OrderDetail;
import main.domain.Product;

public class OrderDetailsConverter implements Converter<ResultSet, OrderDetail> {

	@Override
	public OrderDetail convert(ResultSet source) {
		List<OrderDetail> all = convertAll(source);
		return all.isEmpty() ? null : all.iterator().next();
	}

	@Override
	public List<OrderDetail> convertAll(ResultSet source) {
		
		List<OrderDetail> orders = new LinkedList<>();
		
		try {
			while(source.next()) {
				OrderDetail o = new OrderDetail();
				o.setProductName(source.getString("productName"));
				o.setQuantity(source.getInt("quantityOrdered"));
				o.setPrice(source.getDouble("priceEach"));
				orders.add(o);
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return orders;
	}

}
