package main.converters;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import main.domain.Order;
import main.domain.Product;

public class OrderConverter implements Converter<ResultSet, Order> {

	@Override
	public Order convert(ResultSet source) {
		List<Order> all = convertAll(source);
		return all.isEmpty() ? null : all.iterator().next();
	}

	@Override
	public List<Order> convertAll(ResultSet source) {
		
		List<Order> orders = new LinkedList<>();
		
		try {
			while(source.next()) {
				Order o = new Order();
				o.setCode(source.getString("orderNumber"));
				o.setOrderDate(source.getDate("orderDate"));
				o.setShippedDate(source.getDate("shippedDate"));
				o.setStatus(source.getString("status"));
				o.setComments(source.getString("comments"));
				orders.add(o);
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return orders;
	}

}
