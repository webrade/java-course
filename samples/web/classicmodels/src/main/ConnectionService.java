package main;


import java.sql.Connection;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 * Singleton class which provides connection object.
 */
public class ConnectionService {
	
	private static final ConnectionService INSTANCE = new ConnectionService();
	private DataSource dataSource;
	
	public static void init(DataSource dataSource) {
		INSTANCE.dataSource = dataSource;
	}
	
	private ConnectionService() {
		
	}
	
	public static boolean hasDataSource() {
		return INSTANCE.dataSource != null;
	}
	
	public static Connection getConnection(){
		try {
			return INSTANCE.dataSource.getConnection();
		} catch(SQLException e ) {
			throw new RuntimeException(e);
		}
	}
	
}
