package pages;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.BitSet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import main.AbstractServlet;
import main.ConnectionService;
import main.converters.ProductConverter;

/**
 * Servlet implementation class ProductsServlet
 */
public class ProductsServlet extends AbstractServlet {
	private static final long serialVersionUID = 1L;

	public ProductsServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		Connection con = ConnectionService.getConnection();
		
		String singleProductParam = request.getParameter("uid");
		// check if we have to display all records, or just a single one.
		try {
			if(singleProductParam != null) {
				ResultSet resultSet = con.createStatement().executeQuery("select * from products where productCode='" + singleProductParam + "'");
				request.setAttribute("result", new ProductConverter().convert(resultSet));
				request.setAttribute("uid", singleProductParam);
				request.getRequestDispatcher("/WEB-INF/pages/products_item.jsp").forward(request, response);
			} else {
				ResultSet resultSet = con.createStatement().executeQuery("select * from products");
				request.setAttribute("results", new ProductConverter().convertAll(resultSet));
				request.getRequestDispatcher("/WEB-INF/pages/products.jsp").forward(request, response);
			}
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
