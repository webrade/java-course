package pages;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.BitSet;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import main.AbstractServlet;
import main.ConnectionService;
import main.converters.OrderConverter;
import main.converters.OrderDetailsConverter;
import main.converters.ProductConverter;

/**
 * Servlet implementation class OrdersServlet
 */
public class OrdersServlet extends AbstractServlet {
	private static final long serialVersionUID = 1L;

	public OrdersServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		Connection con = ConnectionService.getConnection();
		
		String singleOrderParam = request.getParameter("uid");
		// check if we have to display all records, or just a single one.
		try {
			if(singleOrderParam != null) {
				ResultSet resultSet = con.createStatement().executeQuery("select * from orders where orderNumber='" + singleOrderParam + "'");
				request.setAttribute("order", new OrderConverter().convert(resultSet));
				resultSet = con.createStatement().executeQuery("select od.quantityOrdered, od.priceEach, p.productName, p.productVendor from orderdetails od INNER JOIN products p ON od.productCode=p.productCode where od.orderNumber='" + singleOrderParam + "'");
				request.setAttribute("results", new OrderDetailsConverter().convertAll(resultSet));
				request.setAttribute("uid", singleOrderParam);
				request.getRequestDispatcher("/WEB-INF/pages/orders_item.jsp").forward(request, response);
			} else {
				ResultSet resultSet = con.createStatement().executeQuery("select * from orders");
				request.setAttribute("results", new OrderConverter().convertAll(resultSet));
				request.getRequestDispatcher("/WEB-INF/pages/orders.jsp").forward(request, response);
			}
			
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
