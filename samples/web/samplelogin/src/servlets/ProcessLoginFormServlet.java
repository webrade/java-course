package servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import domain.User;
import util.Converter;

/**
 * Servlet implementation class ProcessLoginFormServlet
 */
public class ProcessLoginFormServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	DataSource dataSource;
	
	public void init(ServletConfig config) throws ServletException {
		
		InitialContext context;
		try {
			context = new InitialContext();
			dataSource = (DataSource) context.lookup("java:comp/env/jdbc/samplelogin");
			if (dataSource == null)
				throw new ServletException("Unknown DataSource 'jdbc/samplelogin'");
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String password = request.getParameter("password");
		String username = request.getParameter("username");
		String jspView = "WEB-INF/invalidLogin.jsp";
		
		Connection conn;
		try {
			conn = dataSource.getConnection();
			Statement statement = conn.createStatement();

			String searchUserQuery = 
					"SELECT * from users where username='%s' and password='%s'";
			searchUserQuery = String.format(searchUserQuery, username, password);
			ResultSet result = statement.executeQuery(searchUserQuery);
			
			if(result.next()) {
				jspView = "WEB-INF/validLogin.jsp";
				Converter userConverter = new Converter();
				User myUser = userConverter.convert(result);
				request.setAttribute("user", myUser);
				
				List<Integer> numbers = new ArrayList<>();
				numbers.add(1);
				numbers.add(21);
				numbers.add(3);
				numbers.add(4);
				numbers.add(5);
				
				Map<Integer,String> map = new HashMap<>();
				map.put(1, "one");
				map.put(2, "two");
				map.put(3, "three");
				
				int[] arr = new int[]{100, 200, 300, 400};
				
				request.setAttribute("numbers", numbers);
				request.setAttribute("map", map);
				request.setAttribute("arr", arr);
			}

			//conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		RequestDispatcher rd = request.getRequestDispatcher(jspView);
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
