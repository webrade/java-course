<%@ tag language="java" pageEncoding="ISO-8859-1"%>
<%@ taglib tagdir="/WEB-INF/tags/include" prefix="include" %>

<include:header />

<include:leftColumn />

<jsp:doBody />

<include:footer />