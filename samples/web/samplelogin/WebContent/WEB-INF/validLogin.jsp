<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib tagdir="/WEB-INF/tags/template" prefix="template" %>

<template:template>

	<p>Your credentials are VALID;</p>
	
	<p>Founded User is:</p>
	
	<c:if test="${1 != 5}">
		hello World
	</c:if>
	
	<br/>
	<c:choose>
		<c:when test="${1 == 5}">
			hello World 1
		</c:when>
		<c:when test="${2== 5}">
			hello World 1
		</c:when>
		<c:when test="${3 == 5}">
			hello World 1
		</c:when>
		<c:otherwise>
			hello World 2
		</c:otherwise>
	</c:choose>
	
	<ul>
		<li>First name: ${user.firstName}</li>	
		<li>Last name: ${user.lastName}</li>	
		<li>Username: ${user.username}</li>	
		<li>Password: ${user.password}</li>	
		<li>Phone: ${user.phone}</li>
		<li>Phone: ${user.toString()}</li>		
	</ul>
	
	<table border="1">
		<tr>
			<c:forEach begin="1" end="100" var="item">
				<td>${item}</td>
			</c:forEach>
		</tr>
	</table>
	
	<table border="1">
		<tr>
			<c:forEach items="${numbers}" var="item">
				<td>${item}</td>
			</c:forEach>
		</tr>
	</table>
	
	<table border="1">
		<tr>
			<c:forEach items="${arr}" var="item">
				<td>${item}</td>
			</c:forEach>
		</tr>
	</table>
	
	<c:out value="5" />
	<c:out value="${5}" />
	${5}
	<table border="1">
		<tr>
			<c:forEach items="${map}" var="item">
				<td>${item.key}, ${item.value}</td>
			</c:forEach>
		</tr>
	</table>
	
</template:template>