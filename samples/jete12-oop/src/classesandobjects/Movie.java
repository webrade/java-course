package classesandobjects;

public class Movie {
	private static final String DEFAULT_RATING = "5/10";
	
	private static int countOfInstances;
	
	private int id;
	private String title;
	private String rating = DEFAULT_RATING;
	
	static {
		countOfInstances = 0;
	}
	
	public Movie(String title, String rating) {
		this.title = title;
		this.rating = rating;
		countOfInstances++;
		this.id = countOfInstances;
	}
	
	public Movie(String title) {
		this(title, DEFAULT_RATING);
	}

	public String getTitle() {
		return this.title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getRating() {
		return this.rating;
	}
	
	public void setRating(String rating) {
		this.rating = rating;
	}
	
	public void displayDetails() {
		System.out.println("Id: " + id);
		System.out.println("Title: " + title);
		System.out.println("Rating: " + rating);
		System.out.println();
	}
}
