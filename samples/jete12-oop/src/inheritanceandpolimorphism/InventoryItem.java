package inheritanceandpolimorphism;

public class InventoryItem {
	
	protected float price;
	
	public float getPrice() {
		return this.price;
	}
	
	public void setPrice(float price) {
		this.price = price;
	}
}
