package inheritanceandpolimorphism;

public class MovieSortApplication {

	public static void main(String[] args) {
		
		Movie[] movieList = null;
		
		if(movieList == null) {
			movieList = new Movie[] {
					new Movie("The Matrix", 12.3f),
					new Movie("Taxi 3", 8.4f),
					new Movie("Harry Potter 18", 3.27f),
					new Movie("Armageddon", 7.58f)

				};
		}
		System.out.println(movieList.length);
		
		System.out.println("Movies before sorting:");
		for (Movie movie : movieList) {
			movie.displayInfo();
		}
		
		Sort.sortObjects(movieList);

		System.out.println("\nMovies after sorting:");
		for (Movie movie : movieList) {
			movie.displayInfo();
		}		
	}
}

