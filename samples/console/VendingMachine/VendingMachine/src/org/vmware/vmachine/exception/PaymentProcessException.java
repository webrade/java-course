package org.vmware.vmachine.exception;

@SuppressWarnings("serial")
public class PaymentProcessException extends Exception {

	private static final String TEMPLATE_MESSAGE = "Payment can't be processed. Reason: %s ";
	private String reason;
	
	public PaymentProcessException(String reason) {
		super(String.format(TEMPLATE_MESSAGE, reason));
		this.reason = reason;
	}
	
	public String getReason(){
		return this.reason;
	}
}
