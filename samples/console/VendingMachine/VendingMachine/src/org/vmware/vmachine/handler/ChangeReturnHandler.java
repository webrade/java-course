package org.vmware.vmachine.handler;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import org.vmware.vmachine.entity.payment.ChangeReturnResult;
import org.vmware.vmachine.enums.CoinType;
import org.vmware.vmachine.enums.ResultStatus;
import org.vmware.vmachine.exception.CannotCalculateChangeException;
import org.vmware.vmachine.util.PricingUtility;
/**
 * Change return handler.
 * @author Radec
 *
 */
public class ChangeReturnHandler {

	double totalBasketValue;
	double totalInserted;
	Map<CoinType, Integer> availableCash;
	
	public ChangeReturnHandler(double totalBasketValue, double totalInserted, Map<CoinType, Integer> availableCash) {
		this.totalBasketValue = totalBasketValue;
		this.totalInserted = totalInserted;
		setAvailableCash(availableCash);
	}

	public double getTotalBasketValue() {
		return totalBasketValue;
	}

	public void setTotalBasketValue(double totalBasketValue) {
		this.totalBasketValue = totalBasketValue;
	}

	public double getTotalInserted() {
		return totalInserted;
	}

	public void setTotalInserted(double totalInserted) {
		this.totalInserted = totalInserted;
	}

	public Map<CoinType, Integer> getAvailableCash() {
		return availableCash;
	}

	/**
	 * Need sorted map
	 */
	public void setAvailableCash(Map<CoinType, Integer> availableCash) {
		if(!(availableCash instanceof TreeMap)) {
			this.availableCash = new TreeMap<CoinType,Integer>(availableCash);
		} else {
			this.availableCash = availableCash;
		}
	}
	
	private int getAmountInCoins(double value) {
		return PricingUtility.intCoinValue(value);
	}
	
	public ChangeReturnResult calculateChange() throws CannotCalculateChangeException{
		
		if(getTotalInserted() <= getTotalBasketValue() || availableCash == null || availableCash.size() == 0) 
			throw new CannotCalculateChangeException();
		
		double originalValue = getTotalInserted() - getTotalBasketValue();
		//convert into coins
		int valueToBeSearched = getAmountInCoins(originalValue);
		//run change finding algorithm
		Map<CoinType,Integer> result = getChange(valueToBeSearched);
		
		ChangeReturnResult changeResult = new ChangeReturnResult();
		changeResult.setValue(originalValue);
		if(result == null) {
		    changeResult.setResultStatus(ResultStatus.NOK);
			changeResult.setMessage(String.format("Cannot produce %.2f change", originalValue));
		} else {
		    changeResult.setResultStatus(ResultStatus.OK);
			changeResult.setMessage(String.format("Change has been returned. Total : %.2f", originalValue));
			changeResult.setReturnedCoins(result);
		}
		
		return changeResult;
	}
	
	/**
	 * Greedy change finding algorithm.
	 * Return as least coins as possible for the change.
	 * Return as much big coins as possible.
	 * Example:
	 * change to be returned: 1.16
	 * If all coins are available, result will be
	 * 1 x 1.0   instead of    1 x 1.0
	 * 1 x 0.1			       1 x 0.1
	 * 1 x 0.05				   3 x 0.02
	 * 1 x 0.01
	 * 
	 */	
	private Map<CoinType, Integer> getChange(int goalCash) {
		
		int currentCash = 0;
		Map<CoinType, Integer> result = new HashMap<CoinType, Integer>();
		for(Map.Entry<CoinType,Integer> entry : getAvailableCash().entrySet()) {
			
			int check = Math.min(getAvailableAmount(entry.getKey()), (goalCash-currentCash) / entry.getKey().valueInCoins());
			
			if(check < 1) continue;
			
			currentCash += entry.getKey().valueInCoins() * check;
			
			result.put(entry.getKey(),check);
		}
		
		if(currentCash  != goalCash) {
			return null;
		}
		
		return result;
	}

	/**
	 * Return the quantity which is available in the vm for specific coin.
	 * @param coin
	 * @return
	 */
	private int getAvailableAmount(CoinType coin) {
		return availableCash.get(coin);
	}
	
}
