package org.vmware.vmachine.exception;

@SuppressWarnings("serial")
public class InvalidPaymentType extends Exception {
	private static final String TEMPLATE_MESSAGE = "Invalid payment type '%s'. Please re-enter.";
	
	public InvalidPaymentType(String type) {
		super(String.format(TEMPLATE_MESSAGE, type));
	}
}
