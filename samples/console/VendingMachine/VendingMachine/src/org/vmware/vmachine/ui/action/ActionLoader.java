package org.vmware.vmachine.ui.action;

import java.util.HashMap;
import java.util.Map;

import org.vmware.vmachine.ui.action.impl.AddCoinsAction;
import org.vmware.vmachine.ui.action.impl.AddProductAction;
import org.vmware.vmachine.ui.action.impl.CancelOrderAction;
import org.vmware.vmachine.ui.action.impl.DefaultOperationAction;
import org.vmware.vmachine.ui.action.impl.DisplayBasketAction;
import org.vmware.vmachine.ui.action.impl.DisplayBasketTotalAction;
import org.vmware.vmachine.ui.action.impl.DisplayCashTotalAction;
import org.vmware.vmachine.ui.action.impl.DisplayProductsAction;
import org.vmware.vmachine.ui.action.impl.PlaceOrderAction;
import org.vmware.vmachine.ui.action.impl.RemoveProductAction;
import org.vmware.vmachine.ui.action.impl.SetPaymentTypeAction;
import org.vmware.vmachine.ui.action.impl.DisplayOptionsAction;

/**
 * Load all available actions.
 * Defines error, default and menu actions.
 * @author radko
 */
public class ActionLoader {

	private final Map<Integer,Action> availableActions = new HashMap<Integer,Action>();
	private static final ActionLoader INSTANCE = new ActionLoader();
	
	private ActionLoader() {
		if(INSTANCE != null) {
			throw new IllegalStateException("Cannot re-instantiate singleton");
		}
		registerActions();
	}
	
	private void registerActions() {
		availableActions.put(-1,new DefaultOperationAction());
		availableActions.put(0,new DisplayOptionsAction());
		availableActions.put(1,new AddProductAction());
		availableActions.put(2,new RemoveProductAction());
		availableActions.put(3,new SetPaymentTypeAction());
		availableActions.put(4,new AddCoinsAction());
		availableActions.put(5,new DisplayProductsAction());
		availableActions.put(6,new DisplayBasketAction());
		availableActions.put(7,new DisplayCashTotalAction());
		availableActions.put(8,new DisplayBasketTotalAction());
		availableActions.put(9,new PlaceOrderAction());
		availableActions.put(10,new CancelOrderAction());
	}
	
	public static ActionLoader getLoader() {
		return INSTANCE;
	}
	
	public Action load(String key) {
		
		try {
			int value = Integer.parseInt(key);
			if(value > 0 && availableActions.containsKey(value)) {
				return availableActions.get(value);
			}
		} catch(NumberFormatException nfe) {}
		
		return loadErrorAction();
	}
	
	public Action loadInitAction() {
		return availableActions.get(5);
	}
	
	public Action loadDefaultAction() {
		return availableActions.get(0);
	}
	public Action loadErrorAction() {
		return availableActions.get(-1);
	}
}
