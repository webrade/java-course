package org.vmware.vmachine.ui.action.impl;

import static org.vmware.vmachine.util.GUIUtility.displayProducts;
import static org.vmware.vmachine.util.GUIUtility.print;

import org.vmware.vmachine.ui.action.Action;
import org.vmware.vmachine.ui.context.UIContext;

/**
 * Display available products action
 * @author radko
 *
 */
public class DisplayProductsAction implements Action {

	@Override
	public void performAction(UIContext c) {
		
		displayProducts(c.getVmMachine().showAllProducts());
		print("");
	}

}
