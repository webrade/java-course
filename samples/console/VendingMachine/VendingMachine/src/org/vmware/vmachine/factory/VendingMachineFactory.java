package org.vmware.vmachine.factory;

import org.vmware.vmachine.main.VendingMachine;
import org.vmware.vmachine.main.impl.DefaultVendingMachine;

/**
 * Factory for returning @see VendingMachine implementation
 * @author Radec
 *
 */
public class VendingMachineFactory {

	public static VendingMachine getVendingMachine() {
		return new DefaultVendingMachine();
	}
	
}
