package org.vmware.vmachine.exception;

@SuppressWarnings("serial")
public class OrderProcessException extends Exception {

	private static final String TEMPLATE_MESSAGE = "Order can't be processed. Reason : %s . Please alter the order and try again.";

	public OrderProcessException(String reason) {
		super(String.format(TEMPLATE_MESSAGE, reason));
	}
	
	
}
