package org.vmware.vmachine.enums;

/**
 * Represents Available coins
 * @author Radec
 *
 */
public enum CoinType {
	
	HUNDRED(100),FIFTY(50),TWENTY(20),TEN(10),FIVE(5),TWO(2),ONE(1);
	
	private final String strRepresentation;
	private final double value;
	private final int coinValue;
	
	private CoinType(int value) {
		this.coinValue = value;
		this.value = value / 100d;
		this.strRepresentation = String.valueOf(this.value);
	}
	
	@Override
	public String toString() {
		return this.strRepresentation;
	}
	
	public double value() {
		return this.value;
	}
	
	public int valueInCoins() {
		return coinValue;
	}
	
}
