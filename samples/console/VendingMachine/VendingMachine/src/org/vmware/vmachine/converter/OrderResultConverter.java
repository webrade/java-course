package org.vmware.vmachine.converter;

import org.vmware.vmachine.entity.payment.OrderResult;
import org.vmware.vmachine.entity.payment.PaymentResult;

/**
 * Produce @see OrderResult from @see PaymentResult
 * @author Radec
 *
 */
public class OrderResultConverter {
	
	public static OrderResult convert(PaymentResult pr) {
		
		OrderResult result = new OrderResult();
		result.setMessage(pr.getMessage());
		result.setStatus(pr.getStatus());
		result.setChangeAvailable(pr.isChangeReturnNeeded());
		return result;
	}
}
