package org.vmware.vmachine.main;

import java.util.Map;
import java.util.Set;

import org.vmware.vmachine.entity.Product;
import org.vmware.vmachine.enums.CoinType;
/**
 * Abstraction for Storage required by
 * <br> @see VendingMachine to operate
 * @author Radec
 *
 */
public interface Storage {

	/**
	 * Import data in the storage.
	 */
	public void importData();
	
	/**
	 * Add product to the storage
	 */
	public void loadProduct(Product p, int qty);
	/**
	 * Add coins to the storage
	 */
	public void loadCoins(CoinType ct, int qty);
	/**
	 * Retrieve all available products in the storage
	 */
	public Set<Product> getAvailableProducts();
	/**
	 * Check if product is available
	 */
	public boolean isProductAvailable(Product p, int quantity);
	/**
	 * Return product availability
	 */
	public int getAvailableQuantity(Product p);
	/**
	 * Search @see Product by id
	 */
	public Product getProductById(int id);
	/**
	 * Retrieve all available coins
	 */
	public Map<CoinType,Integer> getAvailableCoins();
}
