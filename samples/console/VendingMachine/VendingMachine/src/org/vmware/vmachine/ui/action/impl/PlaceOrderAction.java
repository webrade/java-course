package org.vmware.vmachine.ui.action.impl;

import static org.vmware.vmachine.util.GUIUtility.displayChangeResult;
import static org.vmware.vmachine.util.GUIUtility.print;

import org.vmware.vmachine.entity.payment.OrderResult;
import org.vmware.vmachine.exception.OrderProcessException;
import org.vmware.vmachine.ui.action.Action;
import org.vmware.vmachine.ui.context.UIContext;

/**
 * Place order action
 * @author radko
 *
 */
public class PlaceOrderAction implements Action {

	@Override
	public void performAction(UIContext c) {
		
		try {
			OrderResult result = c.getVmMachine().processOrder();
			print(result.getMessage());
			if(result.getMessageForChange() != null) {
				print(result.getMessageForChange());
			}
			if(result.isChangeAvailable()) {
				displayChangeResult(result.getChange());
			}
		} catch (OrderProcessException e) {
			print(e.getMessage());
		}
	}

}
