package org.vmware.vmachine.ui.action.impl;

import org.vmware.vmachine.entity.payment.OrderResult;
import org.vmware.vmachine.ui.action.Action;
import org.vmware.vmachine.ui.context.UIContext;
import static org.vmware.vmachine.util.GUIUtility.print;

/**
 * Represents cancel order user action
 */
public class CancelOrderAction implements Action {

	@Override
	public void performAction(UIContext c) {
		
		OrderResult result = c.getVmMachine().cancelOrder();
		print(result.getStatus() + ": " + result.getMessage());
	}

}
