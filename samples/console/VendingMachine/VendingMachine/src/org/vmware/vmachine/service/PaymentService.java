package org.vmware.vmachine.service;

import org.vmware.vmachine.entity.OrderState;
import org.vmware.vmachine.entity.payment.PaymentResult;
import org.vmware.vmachine.exception.OrderProcessException;

/**
 * Abstraction for payment service
 * @author radko
 */
public interface PaymentService {


	PaymentResult pay(OrderState orderState) throws OrderProcessException;
}
