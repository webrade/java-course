package org.vmware.vmachine.ui.action.impl;

import static org.vmware.vmachine.util.GUIUtility.print;
import static org.vmware.vmachine.util.GUIUtility.showPrice;

import org.vmware.vmachine.ui.action.Action;
import org.vmware.vmachine.ui.context.UIContext;

/**
 * Shows total added cash action
 * @author radko
 */
public class DisplayCashTotalAction implements Action {

	@Override
	public void performAction(UIContext c) {
		
		showPrice(c.getVmMachine().showCashTotal());
		print("");
	}

}
