package org.vmware.vmachine.ui.action;

import org.vmware.vmachine.ui.context.UIContext;
/**
 * Abstraction for user interaction with the system
 * @author radko
 *
 */
public interface Action {

	void performAction(UIContext ctx);
}
