package org.vmware.vmachine.entity.payment;

import java.util.Map;

import org.vmware.vmachine.enums.CoinType;
import org.vmware.vmachine.enums.ResultStatus;
/**
 * Store information about the change
 * that has to be returned.
 * @author radko
 */
public class ChangeReturnResult {
	
	/**
	 * Status @see ResultStatus
	 */
	ResultStatus resultStatus;
	/**
	 * Message
	 */
	String message;
	/**
	 * Return amount
	 */
	double value;
	/**
	 * Returned coins
	 */
	Map<CoinType, Integer> returnedCoins;
	
	public ResultStatus getResultStatus() {
		return resultStatus;
	}

	public void setResultStatus(ResultStatus resultStatus) {
		this.resultStatus = resultStatus;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public Map<CoinType, Integer> getReturnedCoins() {
		return returnedCoins;
	}

	public void setReturnedCoins(Map<CoinType, Integer> returnedCoins) {
		this.returnedCoins = returnedCoins;
	}

}
