package org.vmware.vmachine.factory;

import org.vmware.vmachine.service.PaymentService;
import org.vmware.vmachine.service.impl.DefaultPaymentService;
/**
 * Factory for returning @see PaymentService implementation
 * @author Radec
 *
 */
public class PaymentServiceFactory {

	public static PaymentService getPaymentService() {
		return new DefaultPaymentService();
	}
}
