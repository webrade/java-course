package org.vmware.vmachine.paymentProviders.impl;

import org.vmware.vmachine.entity.OrderState;
import org.vmware.vmachine.entity.payment.PaymentResult;
import org.vmware.vmachine.enums.ResultStatus;
import org.vmware.vmachine.exception.PaymentProcessException;
import org.vmware.vmachine.paymentProviders.PaymentProvider;

public class CASHPaymentProvider implements PaymentProvider {

	@Override
	public PaymentResult processPayment(OrderState orderState) throws PaymentProcessException {
		if (orderState == null) {
			throw new PaymentProcessException("Invalid order state.");
		}
		double totalBasketValue = orderState.getTotalBasketValue();
		double totalInsertedCash  = orderState.getTotalInsertedCash();
		if (totalBasketValue == 0) {
			throw new PaymentProcessException("No basket items");
		}
		if(totalInsertedCash < totalBasketValue) {
			throw new PaymentProcessException("No enough cash.");
		}
		PaymentResult status = new PaymentResult();
		status.setMessage("Successful cash payment.");
		status.setStatus(ResultStatus.OK);
		if(totalBasketValue < totalInsertedCash) {
			status.setChangeReturnNeeded(true);
		} else {
			status.setChangeReturnNeeded(false);
		}
		return status;
	}

}
