package org.vmware.vmachine.ui.context;

import java.io.Reader;

import org.vmware.vmachine.main.VendingMachine;

/**
 * Loader providing @see UIContext object. 
 * Impose the setup of required data for the context
 */
public class ContextLoader {

	private static final ContextLoader INSTANCE = new ContextLoader();
	private final UIContext context;
	
	private ContextLoader() {
		if(INSTANCE != null) {
			throw new IllegalStateException("Cannot re-instantiate singleton");
		}
		context = new UIContext();
	}
	
	public static ContextLoader getLoader() {
		return INSTANCE;
	}
	
	public UIContext loadContext(VendingMachine vm, Reader r) {
		context.setVmMachine(vm);
		context.setInput(r);
		return context;
	}
	
}
