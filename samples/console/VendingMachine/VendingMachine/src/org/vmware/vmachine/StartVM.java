package org.vmware.vmachine;

import static org.vmware.vmachine.util.GUIUtility.readLine;

import java.io.IOException;
import java.io.Reader;

import org.vmware.vmachine.factory.InputFactory;
import org.vmware.vmachine.factory.VendingMachineFactory;
import org.vmware.vmachine.ui.action.Action;
import org.vmware.vmachine.ui.action.ActionLoader;
import org.vmware.vmachine.ui.context.ContextLoader;
import org.vmware.vmachine.ui.context.UIContext;

/**
 * Run the vending machine
 * @author radko
 */
public class StartVM {

	static final long availableRunningTime = 24 * 60 * 60 * 1000;
	
	public static void main(String[] args) throws IOException {
		start();
	}
	
	private static void start() throws IOException {
		// vm will run for 24 hours and then stop.
		// or when user types EXIT (for simulation)
		long startTime = System.currentTimeMillis();
		
		// Create context loader
		ContextLoader loader = ContextLoader.getLoader();
		// Create reader representing user inputs.
		Reader reader  = InputFactory.createReader(System.in);
		
		// obtain application context object.
		// set required properties to create the context.
		UIContext ctx = loader.loadContext(VendingMachineFactory.getVendingMachine(), reader);
		
		// execute actions based on the application context.
		ActionLoader actionLoader = ActionLoader.getLoader();
		
		// execute initial action
		Action currentAction = actionLoader.loadInitAction();
		currentAction.performAction(ctx);
		
		do {
			if(timeToStop(startTime)) {
				reader.close();
				break;
			}
			currentAction = actionLoader.loadDefaultAction();
			currentAction.performAction(ctx);
			
			String userInput = readLine(reader);
			if(userInput.equals("EXIT") || userInput.equals("exit")) {
				reader.close();
				break;
			}
			currentAction = actionLoader.load(userInput);
			currentAction.performAction(ctx);
			
		} while(true);
		
	}
	
	private static boolean timeToStop(long startTime) {
		if(System.currentTimeMillis() > startTime + availableRunningTime) {
			return true;
		}
		return false;
	}
}
