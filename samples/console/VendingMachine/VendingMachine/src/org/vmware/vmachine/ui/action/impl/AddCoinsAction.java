package org.vmware.vmachine.ui.action.impl;

import org.vmware.vmachine.ui.action.Action;
import org.vmware.vmachine.ui.context.UIContext;

import static org.vmware.vmachine.util.GUIUtility.print;
import static org.vmware.vmachine.util.GUIUtility.readLine;

/**
 * Represents add coins user action
 */
public class AddCoinsAction implements Action {

	@Override
	public void performAction(UIContext c) {
		print("");
		print("Add coins : {COIN;QUANTITY} . For example '5;10' or '100;7'  will result in adding 10 coins of 5p or 7 coins of 1 pound");
		boolean prompt = true;
		while(prompt) {
			try {
				String coins = readLine(c.getInput());
				String[] values = coins.split("[;]");
				c.getVmMachine().addCoins(values[0],Integer.parseInt(values[1]));
				prompt = false;
			} catch (Exception e) {
				print("Please insert valid coin:quantity  pair. Try again.");
			}
		}
	}

}
