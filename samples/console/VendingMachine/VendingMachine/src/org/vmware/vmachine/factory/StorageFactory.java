package org.vmware.vmachine.factory;

import org.vmware.vmachine.main.Storage;
import org.vmware.vmachine.main.impl.DefaultStorage;
/**
 * Factory for returning @see Storage implementation
 * @author Radec
 *
 */
public class StorageFactory {

	public static Storage getStorage() {
		return new DefaultStorage();
	}
}
