package org.vmware.vmachine.main;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

import org.vmware.vmachine.entity.Product;
import org.vmware.vmachine.entity.payment.OrderResult;
import org.vmware.vmachine.exception.InvalidCoinValueException;
import org.vmware.vmachine.exception.InvalidPaymentType;
import org.vmware.vmachine.exception.InvalidQuantityException;
import org.vmware.vmachine.exception.OrderProcessException;

/**
 * Abstraction for VM
 * @author Radec
 */
public interface VendingMachine {
	
	/**
	 * List all available products
	 */
	 Set<Product> showAllProducts();
	 
	 /**
	  * Return current basket state
	  */
	 Map<Product,Integer> showBasket();
	 
	 /**
	  * Add product with respective quantity to the customer's order.
	  * Exception is thrown when quantity not available.
	  */
	 void addProduct(Product product, int qty) throws InvalidQuantityException;
	 
	 /**
	  * Insert coins in the @see VendingMachine
	  */
	 void addCoins(String type,int qty) throws InvalidCoinValueException;
	 
	 /**
	  * Cancel current order
	  */
	 OrderResult cancelOrder();
	 
	 /**
	  * Process order. Evaluate payment.
	  */
	 OrderResult processOrder() throws OrderProcessException;
	 
	 /**
	  * Retrieve available payment types.
	  */
	 Collection<String> getPaymentTypes();
	 
	 /**
	  * Set payment type for order
	  */
	 void setPaymentType(String paymentType) throws InvalidPaymentType;
	 
	 /**
	  * Find product by id
	  */
	 Product getProductById(int id);

	 /**
	  * Remove product from the basket
	  */
	 void removeProduct(int id);
	 
	 /**
	  * Show basket total
	  */
	 double showBasketTotal();
	 
	 /**
	  * Show inserted coins so far.
	  */
	 double showCashTotal();
	 
}
