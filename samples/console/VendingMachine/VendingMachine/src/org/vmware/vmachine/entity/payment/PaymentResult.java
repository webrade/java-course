package org.vmware.vmachine.entity.payment;

import org.vmware.vmachine.enums.ResultStatus;
/**
 * Payment level Result Message
 * @author radko
 */
public class PaymentResult {

	private ResultStatus status;
	private String message;
	private boolean changeReturnNeeded;
	
	public ResultStatus getStatus() {
		return status;
	}

	public void setStatus(ResultStatus status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isChangeReturnNeeded() {
		return changeReturnNeeded;
	}

	public void setChangeReturnNeeded(boolean changeReturnNeeded) {
		this.changeReturnNeeded = changeReturnNeeded;
	}

}
