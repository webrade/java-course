package org.vmware.vmachine.main.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.vmware.vmachine.entity.Product;
import org.vmware.vmachine.enums.CoinType;
import org.vmware.vmachine.main.Storage;
import org.vmware.vmachine.util.DataImporter;
/**
 * Implementation for @Storage
 * Support @see CoinType operations and storage
 * Support @see Product operations and storage
 * 
 * @author Radec
 *
 */
public class DefaultStorage implements Storage {

	private final Map<Product, Integer> vmProductStorage = new HashMap<Product, Integer>();

	private final Map<CoinType, Integer> vmCoinsStorage = new HashMap<CoinType, Integer>();
	
	public DefaultStorage(){
		importData();
	}
	
	@Override
	public void loadProduct(Product p, int qty) {
		if (vmProductStorage.containsKey(p)) {
			vmProductStorage.put(p, vmProductStorage.get(p) + qty);
		} else {
			vmProductStorage.put(p, qty);
		}
	}

	@Override
	public void loadCoins(CoinType ct, int qty) {
		if (vmCoinsStorage.containsKey(ct)) {
			vmCoinsStorage.put(ct, vmCoinsStorage.get(ct) + qty);
		} else {
			vmCoinsStorage.put(ct, qty);
		}
		//remove coins which are not available.
		if(vmCoinsStorage.get(ct) <= 0) {
			vmCoinsStorage.remove(ct);
		}
	}

	@Override
	public Set<Product> getAvailableProducts() {
		Set<Product> avProducts = new TreeSet<Product>();
		for (Product p : vmProductStorage.keySet()) {
			if (vmProductStorage.get(p) > 0) {
				avProducts.add(p);
			}
		}
		return avProducts;
	}

	@Override
	public boolean isProductAvailable(Product p, int quantity) {

		if (vmProductStorage.containsKey(p)
				&& vmProductStorage.get(p) >= quantity) {
			return true;
		}

		return false;
	}

	@Override
	public int getAvailableQuantity(Product p) {
		return vmProductStorage.get(p);
	}

	@Override
	public Product getProductById(int id) {
		for(Product p : getAvailableProducts()) {
			if(p.getId() == id) {
				return p;
			}
		}
		return null;
	}

	@Override
	public Map<CoinType, Integer> getAvailableCoins() {
		return vmCoinsStorage;
	}

	@Override
	public void importData() {
		DataImporter.importCoins(this);
		DataImporter.importProducts(this);
	}
		
}
