package org.vmware.vmachine.exception;

@SuppressWarnings("serial")
public class CannotCalculateChangeException extends Exception {

	private static final String TEMPLATE_MESSAGE = "Cannot calculate change. Please amend the order and try again.";
	
	public CannotCalculateChangeException() {
		super(TEMPLATE_MESSAGE);
	}

}
