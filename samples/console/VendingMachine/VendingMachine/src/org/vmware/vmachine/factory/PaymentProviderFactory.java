package org.vmware.vmachine.factory;

import java.util.HashMap;
import java.util.Map;

import org.vmware.vmachine.paymentProviders.PaymentProvider;
import org.vmware.vmachine.util.GeneralUtility;
/**
 * Factory for returning @see PaymentProvider implementation
 * based on the Payment Type specified
 * @author Radec
 *
 */
public class PaymentProviderFactory {

	private static final Map<String,PaymentProvider> loadedServices = new HashMap<String,PaymentProvider>();
	
	/**
	 * Retrieve a payment service instance based on the selected type.
	 */
	public static PaymentProvider getPaymentProvider(String paymentType) {
		if(paymentType == null) return null;
		
		paymentType = paymentType.toUpperCase();
		String className = new StringBuilder(GeneralUtility.getPaymentStrategyClasspath()).append(paymentType).append(GeneralUtility.getPaymentProviderSuffix()).toString();
		if(loadedServices.containsKey(className)) {
			return loadedServices.get(className);
		}
		try {
			PaymentProvider paymentProvider = (PaymentProvider)Class.forName(className).newInstance();
			loadedServices.put(className, paymentProvider);
			return paymentProvider;
		} catch (InstantiationException e) {} 
		  catch (IllegalAccessException e) {} 
		  catch (ClassNotFoundException e) {}
		
		return null;
	}
	
}
