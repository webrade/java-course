package lecture6.livecoding;

import java.util.Scanner;

public class LiveCoding {
	
	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);
		
		System.out.print("User ages = ");
		int ages = input.nextInt();
		
		if(ages < 19){
			if(ages == 18){
				System.out.println("The user is 18 years old");
			} else{
				System.out.println("The user is younger then 18");
			}
		} else if((ages > 18) && (ages < 40)){
			System.out.println("The user is older then 18, but younger then 40");	
		} else {
			if(ages == 40){
				System.out.println("The user is 40 years old");
			} else{
				System.out.println("The user is older then 40");
			}
		}
			
		input.close();
		
	}
	
	
	
	
	
	
}