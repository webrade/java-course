package abstractClasses;

public class MainClass {

	public static void main(String[] args) {
		
		Animal turtle = new Turtle();
		System.out.printf("The turtle can go %d km/h.%n", turtle.getSpeed());
		
		Cheetah cheetah = new Cheetah();
		System.out.printf("The cheetah can go %d km/h.%n", cheetah.getSpeed());
	}

}
