package transitiveInheritanceExample;

public class Mammal extends Creature {
	private int age;

	public Mammal(int age) {
		this.age = age;
	}

	public int getAge() {
		return this.age;
	}

	public void setAge(int newAge) {
		this.age = newAge;
	}

	public void sleep() {
		System.out.println("shhh. I'm sleeping!");
	}
}
