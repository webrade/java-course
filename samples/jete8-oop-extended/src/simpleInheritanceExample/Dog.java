package simpleInheritanceExample;

//Inherited class Dog
public class Dog extends Mammal {
	private String breed;

	protected int prot;

	public Dog(int age, String breed) {
		super(age);
		this.breed = breed;
	}

	public String getBreed() {
		return this.breed;
	}

	public void setBreed(String breed) {
		this.breed = breed;
	}

	public void wagTail() {
		System.out.println("Tail wagging...");
	}

}
