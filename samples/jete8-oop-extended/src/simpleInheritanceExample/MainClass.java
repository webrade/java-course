package simpleInheritanceExample;

public class MainClass {

	public static void main(String[] args) {
		Dog joe = new Dog(8, "labrador");
		joe.sleep(); // Mammal.sleep() is called
		joe.wagTail(); // Dog.wagTail() is called
		System.out.printf("Joe is %d years old %s.", joe.getAge(), joe
				.getBreed());
		System.out.println(Dog.m);
		System.out.println(Mammal.m);
		Mammal.m = 4;
		System.out.println(Dog.m);
		System.out.println(Mammal.m);
	}

}
