package testing;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

public class Animals {

	
	public static void main(String[] args) throws ParseException, UnsupportedEncodingException {
		
		String test = "радкоdko";
		
		System.out.println(Arrays.toString(test.getBytes("windows-1251")));
		System.out.println(Arrays.toString(test.getBytes("UTF-8")));
		
        String dateString = "11/03/06";
        SimpleDateFormat format = new SimpleDateFormat("MM/dd/yy", Locale.US);
        
        GregorianCalendar c = new GregorianCalendar();
        
        System.out.println(c.get(GregorianCalendar.MONTH));
        
        try {
            Date date = format.parse(dateString);
            System.out.println("Original string for parsing: " + dateString);
            System.out.println("Parsed date: " + date.toString());
        }
        catch(ParseException pe) {
            System.out.println("ERROR: Could not parse date" +
           		"in the string \"" + dateString + "\"");
        }
	}
	
	static abstract class Animal {
		abstract void move();
	}
	
	static class Lion extends Animal {

		@Override
		void move() {
			System.out.println(this.getClass().getSimpleName() + " is running");
		}
		
		void eat() {
			System.out.println("Lion is eating");
		}
	}
	
	public static <T> void asList(T animal) {
		
		List<T> list = new ArrayList<>();
		list.add(animal);
		
	}
	
	static class Frog extends Animal {
		@Override
		void move() {
			System.out.println(this.getClass().getSimpleName() + " is jumping");
		}
	}
	
	static class Butterfly extends Animal {
		@Override
		void move() {
			System.out.println(this.getClass().getSimpleName() + " is flying");
		}
	}
}
