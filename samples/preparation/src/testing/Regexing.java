package testing;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Regexing {

	public static void main(String[] args) {
		
		Pattern pattern = Pattern.compile("([a-zA-Z]*)(\\d+)");
		
		Pattern pattern1 = Pattern.compile(".*\\/app\\/(.*)\\/?");
		
		String start = "D28";
		String end = "D36";
		
		//useMatcher(pattern.matcher(start));
		useMatcher(pattern1.matcher("/sampledata/app/directions/"));
		
		
		System.out.println(new StringBuilder("radko").substring(0, 4));

	}

	private static void useMatcher(Matcher matcher) {
		
		System.out.println(matcher.matches());
		System.out.println(matcher.group(1));
		
		int groupCount = matcher.groupCount();
		
		System.out.println(matcher.group());
		for(int i = 0; i <= groupCount ; i ++) {
			System.out.println(matcher.group(i));
		}
		
	}

}
