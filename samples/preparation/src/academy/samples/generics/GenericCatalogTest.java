package academy.samples.generics;

public class GenericCatalogTest {
	public static void main(String[] args) {
		GenericCatalog<Integer> catalog = new GenericCatalog<Integer>();
		catalog.add(1);
		catalog.add(2);
		catalog.add(3);
		
		Integer[] strArr = catalog.toArray(Integer.class);
		
		for ( Integer element : strArr){
			System.out.println(element);
		}
	}
}
