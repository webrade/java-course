package academy.samples.reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Executable;
import java.lang.reflect.Method;
import java.util.Date;

public class TestReflection {

	
	public static void main(String[] args) {
		
		Man man = new Man();
		
		man.setAge(27);
		man.setDateOfBirth(new Date());
		man.setName("Radko Lyutskanov");
		man.setPower("1000");
		
		Class<? extends Man> manClass = man.getClass();
		
		System.out.println("----class name----");
		//get my class
		System.out.println(manClass.getName());
		
		System.out.println("\n----super class name----");
		// get super class of my object
		System.out.println(manClass.getSuperclass().getName());
		
		System.out.println("\n----methods count----");
		
		// how many methods do I have 
		System.out.println(manClass.getMethods().length);
		
		System.out.println("\n----start methods----");
		
		// what are the names of these methods
		for(Method m : manClass.getMethods()) {
			System.out.println(m.getName() + " has " + m.getParameterCount() + " parameters");
		}
		System.out.println("----end methods----");
		
		System.out.println("\n----constructors----");
		Constructor[] cons = manClass.getConstructors();
		for(Constructor c : cons) {
			System.out.println(c.getName() + " param count: " + c.getParameterCount() + " gen string: " + c.toGenericString());
		}
	}
	
	
}
