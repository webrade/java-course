package academy.samples.reflection;

import java.io.Serializable;

public class Man extends Person implements Serializable, Cloneable {
	
	private String power;
	
	public Man(String test1, String test2) {
		super();
	}
	
	public Man() {
		
	}
	
	private Man(String test) {
		
	}
	
	public String getPower() {
		return power;
	}

	public void setPower(String power) {
		this.power = power;
	}
}
