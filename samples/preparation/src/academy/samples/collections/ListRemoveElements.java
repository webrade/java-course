package academy.samples.collections;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.List;

public class ListRemoveElements {

	
	public static void main(String[] args) {
		
		
		
		List<String> source = new ArrayList<>();
		
		source.add("one");
		source.add("one1");
		source.add("one2");
		source.add("one3");
		source.add("one4");
		
		wrong(source);
		
		correctWithIterator(source);
		
		System.out.println(source);
		
		source.add("one");
		source.add("one1");
		source.add("one2");
		source.add("one3");
		source.add("one4");
		
		best(source);

		
		System.out.println(source);
		
	}

	private static void best(List<String> source) {
		source.removeAll(source);
		
	}

	private static void correctWithIterator(List<String> source) {
		
		Iterator<String> iter = source.iterator();
		
		
		while(iter.hasNext()) {
			iter.next();
			iter.remove();
		}
		
	}

	private static void wrong(List<String> source) {
		
		try {
			// you cannot iterate and remove in the same time.
			for(String item : source) {
				source.remove(item);
			}
		} catch(ConcurrentModificationException ex) {
			System.out.println("Don't do that");
		}
		
	}
	
}
