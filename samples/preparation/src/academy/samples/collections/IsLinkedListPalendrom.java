package academy.samples.collections;

public class IsLinkedListPalendrom {
	
	static class LList<T> {
		
		Node<T> head;
		Node<T> tail;
		int size = 0;
		
		public void add(T value) {
			
			if(head == null) {
				head = new Node<T>();
				head.value = value;
				tail = head;
			} else {
				tail.next = new Node<T>();
				tail.next.value = value;
				tail = tail.next;
			}
			
			size++;
		}
		
		public int size() {
			return size;
		}
		
		public void print() {
			
			Node start = head;
			while(start != null) {
				System.out.println(start.value);
				start = start.next;
			}
		}
		
		public Node<T> get() {
			return head;
		}
		
		public static class Node<T> {
			
			Node<T> next;
			T value;
			
		}
	}
	
	
	public static void main(String[] args) {
		
		LList<Integer> list = new LList<>();
		
		list.add(1);
		list.add(2);
		list.add(3);
		list.add(2);
		list.add(1);
		
		list.print();
	}


}
