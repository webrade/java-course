package academy.samples.workingwithstrings;

public class StringsDemo {
	public static void main(String[] args) {
		String firstName = "Radko";
		String lastName = "Lyutskanov";
		String fullName = firstName + " " + lastName;
		System.out.println(fullName);
		
		String empName = new String("Frank Smith");
		System.out.println("Employee Name = " + empName);
		
		int age = 23;
		System.out.println("Age = " + age);
		
		String str = "Lyutskanov";
		int len = str.length();
		System.out.printf("The length of the string \"%s\" is %d.\n",
			str, len);
		int pos = 1;
		char ch = str.charAt(pos);
		System.out.printf("The char at position %d is '%s'.\n",
			pos, ch);
		
		String s = "How Java processes strings?";
		int start = 4;
		int end = 8;
		String substr = s.substring(start, end);
		System.out.printf("The \"%s\".substring(%d, %d) = \"%s\".\n",
			s, start, end, substr);
		
		String test = "Test";
		System.out.println(test.toLowerCase());
		System.out.println(test.toUpperCase());
		
		String testWithSpaces = "  test  ";
		System.out.println(testWithSpaces.trim());
		
		String javaCourse = "Java Programming Course";
		int index = javaCourse.indexOf("Java"); // index = 0
		System.out.println(index);
		index = javaCourse.indexOf("Course"); // index = 17
		System.out.println(index);
		index = javaCourse.indexOf("COURSE"); // index = -1
		System.out.println(index);
		index = javaCourse.indexOf("ram"); // index = 9
		System.out.println(index);

		String s1 = new String("some string");
		String s2 = new String("some string");
		boolean incorrectCompareResult = (s1 == s2);
		System.out.println(incorrectCompareResult);
		boolean correctCompareResult = (s1.equals(s2));
		System.out.println(correctCompareResult);
		
		String listOfBeers = "Amstel, Zagorka, Tuborg, Becks.";
		String[] beers = listOfBeers.split("[, .]+");
		System.out.println("Available beers are:");
		for (String beer : beers) {
			System.out.printf(" - %s\n", beer);
		}
	}
}
