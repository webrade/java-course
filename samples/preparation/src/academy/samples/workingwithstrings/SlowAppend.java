package academy.samples.workingwithstrings;

public class SlowAppend {
	public static void main(String[] args) {
		String a100000 = dupCharFast('a', 100000);
		System.out.println(a100000.length());
	}
	
	public static String dupChar(char ch, int count) {
	  String result = "";
	  for (int i=0; i<count; i++)
		  result += ch;
	  return result;
	}
	
	public static String dupCharFast(char ch, int count) {
	  StringBuilder result = new StringBuilder(count);
	  for (int i=0; i<count; i++)
		  result.append(ch);
	  return result.toString();
	}

}
