package academy.samples.inputoutput.two;



import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Scanner;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

/**
 * Contains the source codes for the IO slides
 * 
 * @author Milo
 */
public class IODemo {

	public static void systemExample() {
		try {
			Scanner s = new Scanner(System.in);
			System.out.println("Enter integer X");
			int x = s.nextInt();
			System.out.println("Enter integer Y");
			int y = s.nextInt();
			System.out.printf("X / Y = %d%n", x / y);
		} catch (Exception e) {
			e.printStackTrace(System.err);
		}
	}
	
	
	public static void scannerMultiplierExample() {
		Scanner s = new Scanner(System.in);
		BigInteger res = BigInteger.ONE;
		while (s.hasNextBigInteger()) {
			res = res.multiply(s.nextBigInteger());
		}
		System.out.println(res.toString());
	}

	public static void scannerSortExample() {
		Scanner s = new Scanner(System.in);
		ArrayList<String> lines = new ArrayList<String>();
		while (s.hasNextLine()) {
			lines.add(s.nextLine());
		}
		Collections.sort(lines);
		for (String line : lines) {
			System.out.println(line);
		}
	}
	
	public static void printCoord(int x, int y){
		System.out.println("The coordinates are ("+x+", "+y+")");
	}
	
	public static void resourceExample(){
		Scanner s = new Scanner(
				IODemo.class.getResourceAsStream("/resource.txt"));
		while(s.hasNextLine()){
			System.out.println(s.nextLine());
		}
		s.close();
	}
	
	public static void fileTraverseExample(File file, String indent){
		System.out.println(indent + file.getName());
		if(file.isDirectory()){
			File[] items = file.listFiles();
			for(File f : items){
				fileTraverseExample(f, indent + "  ");
			}
		}
	}

	public static void main(String[] args) throws IOException {
		//systemExample();
		//copy("test.zip", "test_copy.zip");
		//scannerMultiplierExample();
		//scannerSortExample();
		//printCoord(5, 13);
		//resourceExample();
		//fileTraverseExample(new File("."), "");		
		//encrypt(new File("resource.txt"), "alabala");
		split(new File("test_copy.zip"), 65536);
		//combine(new File("test_copy.zip"));
		
	}
	
	public static void encrypt(File f, String key) throws IOException{
		RandomAccessFile raf = null;
		try{
			raf = new RandomAccessFile(f, "rw");
			byte[] keyBytes = key.getBytes();
			for(long pos = 0; pos < raf.length(); ++pos){
				byte s = (byte)raf.read();
				s ^= keyBytes[(int)(pos % keyBytes.length)];
				raf.seek(pos);
				raf.write(s);
			}
		}
		finally{
			if(raf != null) {
				raf.close();
			}
		}
	}
	
	/**
	 * Copies up to cout bytes from in to out
	 * This method returns different value from count only if its not possible
	 * to copy all the bytes (EOF occured or an error)
	 * @param in The stream to copy bytes from
	 * @param out The stream to copy bytes to
	 * @param count The amount of bytes to be copied
	 * @return The number of bytes actually copied
	 * @throws IOException 
	 */
	public static long copyBytes(InputStream in, OutputStream out, long count) throws IOException{
		long r = 0;
		while(r < count){
			int b = in.read();
			if(b == -1){
				break;
			}
			out.write(b);
			++r;
		}
		return r;
	}
	
	public static void split(File f, long volumeSize) throws IOException{
		InputStream fi = null;
		try{
			fi = new BufferedInputStream(new FileInputStream(f));
			for(long volNum = 0; ; ++volNum){
				OutputStream fo = null;
				long copied = 0;
				try{
					fo = new BufferedOutputStream(new FileOutputStream(f.getPath()+"."+volNum));
					copied = copyBytes(fi, fo, volumeSize);
				} finally {
					if(fo != null) {
						fo.close();
					}
				}
				if(copied != volumeSize){
					break;
				}
			}
		} finally {
			if(fi != null) {
				fi.close();
			}
		}
	}
	
	public static void combine(File target) throws IOException{
		OutputStream fo = new BufferedOutputStream(new FileOutputStream(target));
		for(long v = 0; ; ++v){
			File vol = new File(target.getPath()+"."+v);
			if(!vol.exists()){
				break;
			}
			InputStream fi = new BufferedInputStream(new FileInputStream(vol));
			copyBytes(fi, fo, vol.length());
			fi.close();
		}
		fo.close();
	}
	
	
}













