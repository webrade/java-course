package academy.samples.inputoutput.one;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class WriterExample {

	public static void main(String[] args) throws IOException {
		Writer writer = new FileWriter("files/input-output/1/file-to-write.txt");
		try {
			writer.write("File writing example.");
		} finally {
			writer.close();
		}
	}

}
