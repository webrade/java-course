package academy.samples.inputoutput.one;

import java.io.FileWriter;
import java.io.PrintWriter;

public class PrintWriterDemo {

	public static void main(String[] args) throws Exception {
		PrintWriter writer = new PrintWriter(new FileWriter("files/input-output/1/numbers.txt", true));
		try {
			for (int i = 1; i <= 100; i++) {
				writer.println(i);
				if (writer.checkError()) {
					throw new Exception("Error writing in the output file.");
				}
			}
		} finally {
			writer.close();
		}
	}
}
