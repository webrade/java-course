package academy.samples.javadocs;

/**
 * @author Radko Lyutskanov
 */
public class HelloJava {

	/**
	 * Sums the elements of a given array of integers.
	 * @param elements The elements to be summed.
	 * @return The sum of the elements.
	 * @throws NullPointerException If elements is null.
	 */
	public int calculateSum(int[] elements) {
		int sum = 0;
		for (int i : elements) {
			sum += i;
		}
		return sum;
	}
	
	/**
	 * Prints "Hello Java!" on the console.
	 * @param args Command line arguments are ignored.
	 */
	public static void main(String[] args) {
		System.out.println("Hello Java!");
	}
}
