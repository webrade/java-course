package academy.samples.datesandi18n;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class CharsetDemo {

	public static void main(String[] args) throws UnsupportedEncodingException {
		String defaultCharset = Charset.defaultCharset().name();
		System.out.println("Current default charset: " + defaultCharset);
		
		String str = "Radko Lyutskanov <-> Радко Люцканов";
		System.out.printf("str = \"%s\"\n", str);
		
		System.out.println((int)'?');
		byte[] encoded = str.getBytes("windows-1251");
		System.out.print("encoded = ");
		printArray(encoded);
		
		String decoded = new String(encoded, "windows-1251");
		System.out.printf("decoded = \"%s\"\n", decoded);
		
	}
	
	private static void printArray(byte[] arr) {
		for (byte b : arr) {
			System.out.printf("%x ", b);
		}
		System.out.println();
	}

}
