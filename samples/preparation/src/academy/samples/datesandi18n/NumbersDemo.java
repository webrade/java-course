package academy.samples.datesandi18n;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

public class NumbersDemo {

	public static void main(String[] args) {
		// Format number as curency examples
		Locale bgLocale = new Locale("bg", "BG");
		NumberFormat bgCurrencyFormat = NumberFormat.getCurrencyInstance(bgLocale);
		System.out.println(bgCurrencyFormat.format(28.50));
		
		// Parse number example
		NumberFormat usNumberFormat = NumberFormat.getNumberInstance(Locale.US);
		String number = "1,292,812.01";
		System.out.println("Original number: " + number);
		try {
			Double parsed = (Double) usNumberFormat.parse(number);
			System.out.println("Parsed number: " + parsed);
		} catch (ParseException pe) {
			pe.printStackTrace();
		}		
	}

}
