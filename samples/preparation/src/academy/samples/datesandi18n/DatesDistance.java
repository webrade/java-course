package academy.samples.datesandi18n;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DatesDistance {

	public static void main(String[] args) throws ParseException {
		
		NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.US);
		
		System.out.println(nf.format(100.50d));
		
		SimpleDateFormat dateFormat = 
			new SimpleDateFormat("dd.MM.yyyy", 
					new Locale("bg"));
		String firstDateStr = "1.01.2008";
		Date firstDate = dateFormat.parse(firstDateStr);
		String secondDateStr = "1.2.2008";
		Date secondDate = dateFormat.parse(secondDateStr);
		
		long firstDateMs = firstDate.getTime();
		long secondDateMs = secondDate.getTime();
		
		long millisecondsPerDay = 24*60*60*1000;
		double days = Math.abs((secondDateMs-firstDateMs) / millisecondsPerDay);
		
		Date today = new Date();
		long todayMs = today.getTime(); 
		Date nextDay = new Date(todayMs+millisecondsPerDay);
		
		System.out.println(nextDay);
		System.out.println((int)days);
		
	}

}
