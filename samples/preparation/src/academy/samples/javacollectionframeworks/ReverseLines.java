package academy.samples.javacollectionframeworks;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.Scanner;

public class ReverseLines {

	public static void main(String[] args) {
		LinkedList<String> list = new LinkedList<String>();
		Scanner scanner = null;

		try {
			scanner = new Scanner(new File("files/collections/Reversed.txt"));

			while (scanner.hasNextLine()) {
				list.add(scanner.nextLine());
			}

			while (list.size() > 0) {
				System.out.println(list.removeLast());
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			if (scanner != null) {
				scanner.close();
			}
		}
	}
}
