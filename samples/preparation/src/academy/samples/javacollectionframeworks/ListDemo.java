package academy.samples.javacollectionframeworks;

import java.util.*;

public class ListDemo {

	
	public static void main(String[] args) {
		
		// Here we construct our list using LinkedList implementation
		List<String> list = new LinkedList<String>();		
		// However we can use any class which implements List interface
		

		// We can add items
		list.add("Zagorka");		
		list.add("Ariana");
		list.add("Shumensko");
		list.add("Kamenitza");
		
		// We can check if item exists in our list
		System.out.println(list.contains("Kamenitza")); //true
		System.out.println(list.contains("Guinnes")); //false
		
		// We can remove an item
		list.remove("Ariana");
		
		// We can get item's index and set it with a new value		
		list.set(list.indexOf("Zagorka"), "Pirinsko");
		
		
		// We can iterate it with the new foreach loop
		for(String s : list) {
			System.out.println(s);
		}
		
		// The upper is equivelent to this
		Iterator iterator = list.iterator();	
		while(iterator.hasNext()) {
			System.out.println(iterator.next());
		}
	}

}
