package academy.samples.xmlprocessing;

import java.io.FileNotFoundException;
import java.io.FileReader;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

public class StaxXmlReader {
	public static void main(String[] args) throws FileNotFoundException,
			XMLStreamException {
		FileReader fileReader = new FileReader("xml/Students.xml");
		XMLInputFactory factory = XMLInputFactory.newInstance();

		XMLStreamReader reader = factory.createXMLStreamReader(fileReader);
		String element = "";
		while (reader.hasNext()) {

			if (reader.isStartElement()) {
				element = reader.getLocalName();

			} else if (reader.isCharacters() && !reader.isWhiteSpace()) {
				System.out.printf("%s - %s%n", element, reader.getText());
			}
			reader.next();
		}
		reader.close();
	}
}
