package academy.samples.xmlprocessing;

import java.io.FileOutputStream;
import java.io.IOException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.sun.org.apache.xml.internal.serialize.OutputFormat;
import com.sun.org.apache.xml.internal.serialize.XMLSerializer;

public class ModifyingXML {
	
	private static final String INPUT_FILE_NAME = "xml/items.xml";
	private static final String OUTPUT_FILE_NAME = "xml/newItems.xml";

	public static void main(String[] args) throws ParserConfigurationException,
	SAXException, IOException, XPathExpressionException, ParseException {
		// Get a DocumentBuilder object
		DocumentBuilderFactory dbf =
			DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = dbf.newDocumentBuilder();

		// Parse XML document and build DOM tree
		Document doc = docBuilder.parse(INPUT_FILE_NAME);
		
		// Get XPath evaluator
		XPathFactory xpFactory = XPathFactory.newInstance();
		XPath xpath = xpFactory.newXPath();

		// Find all nodes that contain price of a beer and raise the price 20%
		NodeList beerPriceNodes = (NodeList) xpath.evaluate(
			"/items/item[@type='beer']/price", doc, XPathConstants.NODESET);
		for (int index=0; index<beerPriceNodes.getLength(); index++) {
			Node beerPriceNode = beerPriceNodes.item(index);
			String priceStr = beerPriceNode.getTextContent();
			NumberFormat usFormat = NumberFormat.getNumberInstance(Locale.US);
			double price = usFormat.parse(priceStr).doubleValue();
			double newPrice = price*1.2;
			String newPriceStr = NumberFormat.getInstance(
					Locale.US).format(newPrice);
			beerPriceNode.setTextContent(newPriceStr);
		}

		// Save the modified document to a new file
		XMLSerializer xmlSer = new XMLSerializer();
		FileOutputStream fileOut = new FileOutputStream(OUTPUT_FILE_NAME);
		try {
			xmlSer.setOutputByteStream(fileOut);
			OutputFormat format = new OutputFormat();
			format.setEncoding("koi8-r");
			xmlSer.setOutputFormat(format);
			xmlSer.serialize(doc);
		} finally {
			fileOut.close();
		}
	}

}
