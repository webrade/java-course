package academy.samples.xmlprocessing;


import java.io.FileWriter;
import java.io.IOException;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

public class StaxXmlWriter
{
    public static void main(String args[])
        throws IOException, XMLStreamException
    {
        String fileName = "Customers.xml";
        FileWriter fileWriter = new FileWriter(fileName);  
        XMLOutputFactory factory = XMLOutputFactory.newInstance();

        XMLStreamWriter writer = 
        	factory.createXMLStreamWriter(fileWriter);
        try {
	        writer.writeStartDocument();
		        writer.writeStartElement("Customers");
			        writer.writeStartElement("Customer");
				        writer.writeStartElement("Name");
				        writer.writeCharacters("ABC < Pizza");
				        writer.writeEndElement();
				        writer.writeStartElement("Address");
				        writer.writeCharacters("1 Main Street");
				        writer.writeEndElement();
				        writer.writeStartElement("City");
				        writer.writeCharacters("Simsbury");
				        writer.writeEndElement();
				        writer.writeStartElement("State");
				        writer.writeCharacters("CT");
				        writer.writeEndElement();
				        writer.writeStartElement("Zip");
				        writer.writeCharacters("06070");
				        writer.writeEndElement();
			        writer.writeEndElement();
		        writer.writeEndElement();
			writer.writeEndDocument();
	        writer.flush();
        } finally {
            writer.close();
        }
    }
}
