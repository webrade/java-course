package academy.samples.xmlprocessing;

import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.sun.org.apache.xml.internal.serialize.OutputFormat;
import com.sun.org.apache.xml.internal.serialize.XMLSerializer;

public class BuildingXMLDocument {

	public static void main(String[] args) 
	throws ParserConfigurationException, IOException {
		// Get a DocumentBuilder object
		DocumentBuilderFactory dbf =
			DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = dbf.newDocumentBuilder();

		// Get new empty Document from DocumentBuilder
		Document doc = docBuilder.newDocument();

		// Create a new <dots> element and add to the document as root
		Element root = doc.createElement("dots");
		doc.appendChild(root);

		// Create a new <dot> element and add as child of root
		Element dot = doc.createElement("dot");
		dot.setAttribute("x", "9");
		dot.setAttribute("y", "81");
		root.appendChild(dot);
		
		appendNewElement(doc, dot, "name", "pesho");
		appendNewElement(doc, dot, "color", "red");
		appendNewElement(doc, dot, "name", "Radko");
		appendNewElement(doc, dot, "name", "радко");
		appendNewElement(doc, dot, "name", "радко 2");

		// Create a new <dot> element and add as child of root
		Element dot2 = doc.createElement("dot");
		dot2.setAttribute("x", "99999");
		dot2.setAttribute("y", "811111");
		root.appendChild(dot2);
		
		// Print the created document to the console
		System.out.println("Parsed XML document into DOM tree:");
		OutputFormat format = new OutputFormat(doc, "utf-8", true);
		format.setIndent(4);
		XMLSerializer xmlSer = new XMLSerializer(System.out, format);
		xmlSer.serialize(doc);
	}

	private static void appendNewElement(
			Document doc, Element parentNode, String nodeName,
			String nodeText) {
		Element newElement = doc.createElement(nodeName);
		newElement.setTextContent(nodeText);
		parentNode.appendChild(newElement);
	}

}
