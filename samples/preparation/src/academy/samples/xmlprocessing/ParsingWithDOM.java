package academy.samples.xmlprocessing;

import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.sun.org.apache.xml.internal.serialize.XMLSerializer;

public class ParsingWithDOM {

	public static void main(String[] args) 
	throws ParserConfigurationException, SAXException, IOException {
		// Get a DocumentBuilder object
		DocumentBuilderFactory dbf =
			DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = dbf.newDocumentBuilder();
		
		// Parse XML document and build DOM tree
		Document doc = docBuilder.parse("xml/library.xml");

		// Print the parsed document
		System.out.println("Parsed XML document into DOM tree:");
		XMLSerializer xmlSer = new XMLSerializer();
		xmlSer.setOutputByteStream(System.out);
		xmlSer.serialize(doc);
		System.out.println();
		System.out.println();

		// Print the root element
		Element rootElement = doc.getDocumentElement();
		System.out.println("Root element: " + rootElement.getTagName());

		// Print the attributes of the root element
		NamedNodeMap attributes = rootElement.getAttributes();
		for (int i=0; i<attributes.getLength(); i++)
		{
			Attr attribute = (Attr) attributes.item(i);
			System.out.printf("Attribute: %s=%s\n",
					attribute.getName(), attribute.getValue());
		}
		System.out.println();

		// Print the childeren of the root element
		NodeList rootChildren = rootElement.getElementsByTagName("book");
		for (int i=0; i<rootChildren.getLength(); i++)
		{
			Node rootChild = rootChildren.item(i);
			System.out.println("Root child element: " + rootChild.getNodeName());

			// Print the childeren of current element
			NodeList children = rootChild.getChildNodes();
			for (int j=0; j<children.getLength(); j++)
			{
				Node node = children.item(j);
				if (node.getNodeType() == Node.ELEMENT_NODE) {
					System.out.printf("  Child element: %s=%s\n",
						node.getNodeName(), node.getTextContent());
				}
			}
		}
	}

}
