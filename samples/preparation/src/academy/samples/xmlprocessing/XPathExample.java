package academy.samples.xmlprocessing;

import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class XPathExample {

	public static void main(String[] args) throws ParserConfigurationException,
	SAXException, IOException, XPathExpressionException {
		// Get a DocumentBuilder object
		DocumentBuilderFactory dbf =
			DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = dbf.newDocumentBuilder();

		// Parse XML document and build DOM tree
		Document doc = docBuilder.parse("xml/items.xml");
		
		// Get XPath evaluator
		XPathFactory xpFactory = XPathFactory.newInstance();
		XPath xpath = xpFactory.newXPath();
		
		// Extract single tag values with XPath
		String item2name = xpath.evaluate("/items/item[1]/name", doc);
		System.out.println("Item 2 name = " + item2name);
		
		String item2price = xpath.evaluate("/items/item[2]/price", doc);
		System.out.println("Item 2 price = " + item2price);
		
		// Extract multiple tags values with XPath
//		XPathExpression xpathExpr = 
//			xpath.compile("/items/item[@type='beer']");
//		NodeList nodes = (NodeList) xpathExpr.evaluate(doc, XPathConstants.NODESET);

		NodeList nodes = (NodeList) xpath.evaluate(
			"/items/item[@type='beer']", doc, 
			XPathConstants.NODESET);
		
		for (int index=0; index<nodes.getLength(); index++) {
			Node beerNode = nodes.item(index);
			
			// Address relative content with XPath (starting from particular node)
			String name = xpath.evaluate("name", beerNode);
			String price = xpath.evaluate("price", beerNode);
			System.out.println("Beer name: " + name);
			System.out.println("Beer price: " + price);
		}
	}

}
