package academy.samples.exceptions;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ExceptionsDemo {
	public static void main(String[] args) {
		try {
			
			
			System.out.print("Please enter an integer number: ");
			Scanner in = new Scanner(System.in);
			int value = in.nextInt();
			System.out.println("You entered: " + value);
			
			
			
		} catch (InputMismatchException mismatchEx) {
			System.out.println("Invalid integer number!");
		} finally {
			System.out.println("Program finished.");
		}
	}
}
