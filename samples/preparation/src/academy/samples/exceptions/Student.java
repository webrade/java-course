package academy.samples.exceptions;

public class Student {
	private static final int UNKNOWN_AGE = -1;
	private static final int MIN_AGE = 1;
	private static final int MAX_AGE = 50;
	
	private String name;
	private int age;
	
	public Student(String name, int age) {
		if (name == null) {
			throw new IllegalArgumentException("Name can not be null!");
		}
		
		if (name.length() == 0) {
			throw new IllegalArgumentException("Invalid name!");
		}
		
		if ( ((age<MIN_AGE) || (age>MAX_AGE)) && (age != UNKNOWN_AGE)) {
			throw new IllegalArgumentException("Invalid age!");
		}
				
		this.name = name;
		this.age = age;		
	}

	public Student(String name) {
		this(name, UNKNOWN_AGE);
	}
	
	public String getName() {
		return this.name;
	}
	
	public int getAge() {
		return this.age;
	}
}
