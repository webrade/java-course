package academy.samples.workingwithobjects.point;

public class PointTest {

	public static void main(String[] args) {
		Point p1 = new Point(3, 5);
		Point p2 = new Point(4, 1);
		Point p3 = new Point(4, 1);

		// Equality
		System.out.println(p2.equals(p3)); // true
		// Identity
		System.out.println(p2 == p3); // false
		// The upper variables reference different objects, however they are
		// logically equal

		// Object Hashcode
		System.out.println(p2.hashCode()); //

		// Hashcode is int and therefore can be compared with '=='
		System.out.println(p2.hashCode() == p3.hashCode()); // true
		// The hashcode function MUST produce the same hashcode for logically
		// equal objects.

		System.out.println(p1.hashCode() == p2.hashCode()); // false
		// And in the perfect case, each pair of different objects must have
		// different hashcode. However, that is rarely possible

	}

}
