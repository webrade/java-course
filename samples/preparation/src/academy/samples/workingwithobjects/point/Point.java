package academy.samples.workingwithobjects.point;

public class Point {
	private int x;
	private int y;

	public Point(int x, int y) {
		super();
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public boolean equals(Object obj) {
		// correct behavior on null
		if (obj instanceof Point) {
			Point o = (Point) obj;
			return x == o.x && y == o.y;
		}
		return false;
	}

	public int hashCode() {
		return x * 20143 ^ y * 10169;
	}
}
