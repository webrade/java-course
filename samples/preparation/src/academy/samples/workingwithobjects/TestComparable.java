package academy.samples.workingwithobjects;

import java.util.Arrays;

public class TestComparable {

	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		
		Student[] students = new Student[4];

		students[0] = new Student();
		students[0].setFirstName("Todor");
		students[0].setLastName("Ivanov");
		students[0].setAge(22);

		students[1] = new Student();
		students[1].setFirstName("Valentin");
		students[1].setLastName("Ivanov");
		students[1].setAge(25);

		students[2] = new Student();
		students[2].setFirstName("Mario");
		students[2].setLastName("Peshev");
		students[2].setAge(20);

		students[3] = new Student();
		students[3].setFirstName("Vasil");
		students[3].setLastName("Stoqnov");
		students[3].setAge(21);

		for (Student s : students) {
			System.out.println(s.getFirstName() + " " + s.getLastName() + ":"
					+ s.getAge());
		}
		Arrays.sort(students);
		System.out.println();
		System.out.println("Sorted students by age");
		System.out.println();
		
		for (Student s : students) {
			System.out.println(s.getFirstName() + " " + s.getLastName() + ":"
					+ s.getAge());
		}
		System.out.println();
		System.out.println("Sorted students by last name");
		Arrays.sort(students,new LastNameComparator());
		for (Student s : students) {
			System.out.println(s.getFirstName() + " " + s.getLastName() + ":"
					+ s.getAge());
		}
	}

}
