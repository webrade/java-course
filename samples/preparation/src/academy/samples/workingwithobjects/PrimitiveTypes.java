package academy.samples.workingwithobjects;

public class PrimitiveTypes {

	public static void main(String[] args) {
		int i = 5;
		// float f = 5.5; error
		float f = 5.5f;
		// i = f; error

		i = (int) f; // ok, i = 5
		System.out.println(i);

		f = i; // ok, f=5
		System.out.println(f);

		byte b = 5;
		short s = 10;
		// b = b + b; error
		// b=(byte) b + b; still error

		b = (byte) (b + b); // ok
		System.out.println(b);

		// s=b+b;error

		s = (short) (b + b); // ok
		System.out.println(s);

		double d = f; // ok
		d = b; // ok

		System.out.println(d);
		char c = 'A';
		// s = c; error

		i = c; // ok,i=65
		System.out.println(i);

		i = c * c * c; // oki=274625
		System.out.println(i);

		long l = i * i; // bad,l=-1890520703
		System.out.println(l);

		long l2 = (long) i * i; // l2=75418890625
		System.out.println(l2);

		float f2 = l2; // f2=75418894336
		System.out.println(f2);
	}
}
