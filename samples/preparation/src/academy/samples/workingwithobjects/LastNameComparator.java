package academy.samples.workingwithobjects;

import java.util.Comparator;


public class LastNameComparator implements Comparator{
  public int compare(Object student1, Object student2) {
    String lastName1 = ((Student) student1).getLastName().toUpperCase();
    String lastName2 = ((Student) student2).getLastName().toUpperCase();

    return lastName1.compareTo(lastName2);
  }
}
