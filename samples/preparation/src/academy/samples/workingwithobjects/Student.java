package academy.samples.workingwithobjects;

public class Student implements Comparable<Student>{
	private String firstName;
	private String lastName;

	private int age;

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Override
	public int compareTo(Student o) {
		if(this.age < o.age) {
			return 1;
		} else if (this.age == o.age) {
			return 0;
		} else {
			return -1;
		}
	}

	@Override
	public String toString() {
		return "Student [firstName=" + firstName + ", lastName=" + lastName + ", age=" + age + "]";
	}

	

}
