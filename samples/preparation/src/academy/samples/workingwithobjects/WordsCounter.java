package academy.samples.workingwithobjects;

import java.util.Map;
import java.util.TreeMap;

public class WordsCounter {

	public static void main(String[] args) {
		String text = "Plovdiv Academy for Software Development is a"
				+ "private educational centre for training highly qualified"
				+ "software developers for the needs of the IT industry";
		String[] words = text.split("(\\W)+");

		// Declaring and Initializing TreeMap as Map interface to store the
		// words
		Map<String, Integer> m = new TreeMap<String, Integer>();

		// Iterating through the words
		for (String word : words) {
			Integer freq = m.get(word);
			m.put(word, freq == null ? 1 : freq + 1);
		}

		// Printing the contents of the Map. Here the TreeMap's toString
		// function is automatically called
		System.out.println(m);
	}

}
