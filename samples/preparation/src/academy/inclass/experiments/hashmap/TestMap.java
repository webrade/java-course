package academy.inclass.experiments.hashmap;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TestMap {

	public static void main(String[] args) {
		
		
		Movie movie1 = new Movie();
		movie1.setDuration(100);
		movie1.setRating("9/10");
		movie1.setTitle("HarryPotter1");
		
		Movie movie2 = new Movie();
		movie2.setDuration(100);
		movie2.setRating("10/10");
		movie2.setTitle("HarryPotter");
		
		System.out.println(movie1.equals(movie2));
		System.out.println(movie2.hashCode());
		
		Map<Movie, Integer> views = new HashMap<Movie, Integer>();
		views.put(movie1, 1_000_001);
		views.put(movie2, 2_000_001);
		
		Collection<Integer> milionViews = views.values();
		
		for(Integer val : milionViews) {
			System.out.println(val);
		}
		
		movie1.setTitle("test");
		System.out.println(movie1);
		System.out.println(views.get(movie1));
		
		System.out.println(views.get(movie1));
		System.out.println(views.get(movie2));
		
		List<Movie> movies = new ArrayList<>();
		
		movies.add(movie1);
		movies.add(movie2);

		movies.remove(movie2);
		
		System.out.println(movies.get(0));
		
	}

}
