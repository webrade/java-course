package academy.inclass.experiments.oop;

public class Figure {
	
	public Figure() {
		System.out.println("creating figure");
	}
	
	public double getSurface() {
		System.out.println("default surface");
		return 0d;
	}
}
