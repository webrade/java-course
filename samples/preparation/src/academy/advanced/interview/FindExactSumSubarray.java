package academy.advanced.interview;

import java.util.HashMap;
import java.util.Map;

public class FindExactSumSubarray {
	
	static void tryInt(Integer source) {
		source = source + 1;
	}
	public static void main(String[] args) {
		
		
		Integer a = 100;
		
		tryInt(a);
		
		System.out.println(a);
//		System.out.println(a);
//		
//		Integer i = 100;
//		Integer i1 = 100;
//		
//		System.out.println(i == i1);
//		
//		
//		String a = "a";
//		String a1 = "a";
//		
//		System.out.println(a == a1);
		
		//true
		//System.out.println(find(new int[]{1,2,3,4,5,6}, 12));
		
		// false
		//System.out.println(find(new int[]{1,2,3,4,5,6}, 13));
	}

	
	static boolean find(int[] source, int search) {
		
		
		Map<Integer, Integer> storage = new HashMap<>();
		
		int sum = 0;
		for(int i = 0 ; i < source.length ; i ++) {
			
			sum += source[i];
			
			if(sum == search || storage.containsKey(sum - search)) {
				return true;
			}
			
			storage.put(sum, i);
		}
		
		System.out.println(storage);
		return true;
		
	}
}
