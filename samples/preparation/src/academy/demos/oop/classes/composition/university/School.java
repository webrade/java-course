package academy.demos.oop.classes.composition.university;

public class School {
	
	private StudentClass[] classes;

	public StudentClass[] getClasses() {
		return classes;
	}

	public void setClasses(StudentClass[] classes) {
		this.classes = classes;
	}
	
	
}
