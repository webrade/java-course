package academy.demos.oop.classes.composition.university;

public class Teacher {
	
	private String name;
	private StudentClass[] classes;
	private Discipline[] disciplnes;
	
	public StudentClass[] getClasses() {
		return classes;
	}

	public void setClasses(StudentClass[] classes) {
		this.classes = classes;
	}

	public Discipline[] getDisciplnes() {
		return disciplnes;
	}

	public void setDisciplnes(Discipline[] disciplnes) {
		this.disciplnes = disciplnes;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
