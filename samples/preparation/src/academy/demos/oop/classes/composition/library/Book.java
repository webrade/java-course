package academy.demos.oop.classes.composition.library;

public class Book {

	private String title;
	private int numberOfPages;
	private String genre;
	private Author author;

	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getNumberOfPages() {
		return numberOfPages;
	}

	public void setNumberOfPages(int numberOfPages) {
		this.numberOfPages = numberOfPages;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	@Override
	public String toString() {
		return "Book [title=" + title + ", numberOfPages=" + numberOfPages + ", genre=" + genre + ", author=" + author
				+ "]";
	}

	
	
}
