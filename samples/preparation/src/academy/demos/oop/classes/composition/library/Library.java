package academy.demos.oop.classes.composition.library;

import java.util.Arrays;

public class Library {
	
	private String name;
	private String location;
	private Book[] books;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public Book[] getBooks() {
		return books;
	}
	public void setBooks(Book[] books) {
		this.books = books;
	}
	
	public void displayCatalog() {
		for(Book book : getBooks()) {
			System.out.println(book);
		}
	}
	
	@Override
	public String toString() {
		return "Library [name=" + name + ", location=" + location + ", books=" + Arrays.toString(books) + "]";
	}
	
	
}
