package academy.demos.oop.classes.composition.university;

public class App {

	public static void main(String[] args) {
		
		School school = new School();
		
		StudentClass[] classes = new StudentClass[1];
		Student[] students = new Student[1];
		Student student = new Student();
		student.setName("Radko");
		students[0] = student;
		
		StudentClass sc1 = new StudentClass();
		sc1.setStudents(students);
		classes[0] = sc1;
		
		school.setClasses(classes);
		
		
		
		school.setClasses(classes);
		

	}

}
