package academy.demos.oop.classes.testing;

public class OuterClass {
	
	public String test;
	
	class InnerClass {
		
		public String doIt() {
			return test;
		}
		
	}
	
	public static void main(String[] args) {
		new OuterClass().new InnerClass().doIt();

	}

}
