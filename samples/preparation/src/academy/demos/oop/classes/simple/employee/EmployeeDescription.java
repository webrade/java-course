package academy.demos.oop.classes.simple.employee;

/**
 * That class will represents an Employee in real life
 * - Person that is working for particular company
 * 
 * @author radkol
 *
 */
public class EmployeeDescription {
	
	/**
	 * When creating employee what do we need as its "state"
	 * 
	 * Name - What is his name
	 * Age - What is his age
	 * years of Experience - How many years
	 * salary - what is his monthly salary 
	 * workHours - how many hours does he work per day
	 */
	 //TODO define attributes
	
	
	/**
	 * This example won't use constructors
	 * We are going to use the default Java constructor.
	 * Hence we don't have to write it.
	 */
	
	/**
	 * We are going to control the state of the type via property methods
	 * and behaviour methods
	 */
	// TODO create get and set methods
	
	
	/**
	 * Once we define the state of the type, lets see what operations do we need
	 * so we can use that class.
	 * For example:
	 * We want to create employees and display personal info
	 * We want to receive info of that how much a particular person is earning per hour ?
	 */
	 
	
	/**
	 * Override toString() method inherited from Object type.
	 * toString() method gives string representation of an object.
	 * so if we print that object, we receive valuable information about it, not just its address in the memory.
	 */
	
	
}
