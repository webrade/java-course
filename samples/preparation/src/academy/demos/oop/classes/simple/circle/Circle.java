package academy.demos.oop.classes.simple.circle;

public class Circle {

	private double radius;
	private String color;
	
	public Circle() {
		radius = 4d;
		color = "red";
	}
	
	public Circle(double radius) {
		this();
		this.radius = radius;
	}
	
	public Circle(double radius, String color) {
		this.radius = radius;
		this.color = color;
	}

	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public void getCircleArea() {
		// pi * r * 2
		
	}
	
	public void getCircleSurface() {
		// pi * r * r
	}
	
	@Override
	public String toString() {
		return "Circle [radius=" + radius + ", color=" + color + "]";
	}
	
	
	
	
}
