package academy.demos.oop.classes.simple.employee;

public class UseEmployee {
	
	public static void main(String[] args) {
		
		Employee employee1 =  new Employee();
		
		employee1.setAge(27);
		employee1.setName("Teodora");
		employee1.setSalary(5000d);
		
		System.out.println(employee1.toString());
		
		Employee employee2 =  new Employee();
		employee2.setAge(29);
		employee2.setName("Radko");
		employee2.setSalary(1000d);
		
		double hoursRate = employee1.calculateHourRate();
		double hoursRate2 = employee2.calculateHourRate();
		
		
		System.out.printf("Our first employee %s, age %d earn %f per hour", employee1.getName(), employee1.getAge(), hoursRate);
		System.out.printf("\nOur second employee %s, age %d earn %f per hour", employee2.getName(), employee2.getAge(), hoursRate2);
		
		
		
		
		
		
		
		
		
//		// array which will hold 3 employees
//		Employee[] employees = new Employee[3];
//		
//		
//		Employee emp1 = new Employee();
//		Employee emp2 = new Employee();
//		Employee emp3 = new Employee();
//		
//		employees[0] = emp1;
//		employees[1] = emp2;
//		employees[2] = emp3;
//		
//		for(Employee e: employees) {
//			System.out.println(e.toString());
//		}
//		
//		System.out.println("-------------");
//		
//		System.out.println(emp1.toString());
//		System.out.println(emp2.toString());
//		System.out.println(emp3.toString());

	}

}
