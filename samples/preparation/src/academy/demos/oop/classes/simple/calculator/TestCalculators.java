package academy.demos.oop.classes.simple.calculator;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class TestCalculators {

	public static void main(String[] args) {
		
		String[] source = 
			{"a" , "b", "a", "b", "c", "a"};
		
		Map<String, Integer> storage = new TreeMap<>();
		
		for(String word : source) {
			
			Integer currentWordCount = storage.get(word);
			if(currentWordCount == null) {
				storage.put(word, Integer.valueOf(1));
			} else {
				storage.put(word, 1 + currentWordCount);
			}
			
		}
		
		
		System.out.println(storage);
		
		
		
		Long counter = 0l;
		
		for(int i = 100; i < 10000000; i ++) {
			counter += i;
		}
		
		
		long count = 0l;
		
		for(int i = 100; i < 10000000; i ++) {
			count += i;
		}
		
		System.out.println(counter);
		System.out.println(count);
		
		List list = new ArrayList();
		
		list.add(5);
		
		
		float f = 5.5f;
		short sh = (short)666666;
		byte b = 10;
		
		byte newByte = (byte)(b + (byte)sh);
		
		
		System.out.println(sh);
		Animal a1 = new Cat();
		
	}
	
	
	static class Animal {
		
	}
	
	static class Cat extends Animal{
		
		
		public void miau() {
			System.out.println("Miau");
		}
	}
	
	static class Frog extends Animal{
		
	}
}
