package academy.demos.mathandprocess;

public class Fibonacci {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.out.println(fibonacciIterative(7));
		System.out.println(fibonacciRecursive(7));
	}
	
	// 1 1 2 3 5 8 13
	public static long fibonacciIterative(int number) {
		
		int current = 1;
		int previous = 1;
		
		if(number < 2) {
			return current;
		}
		
		for(int i = 1 ; i < number ; i ++) {
			int temp = current;
			current += previous;
			previous = temp;
		}
		
		return current;
	}
	
	public static long fibonacciRecursive(int number) {
		
		if(number < 2) {
			return 1;
		}
		
		return fibonacciRecursive(number - 1) + fibonacciRecursive(number-2);
	}

}
