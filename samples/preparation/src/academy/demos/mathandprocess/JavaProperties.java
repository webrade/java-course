package academy.demos.mathandprocess;

import java.util.Properties;
import java.util.Set;
import java.util.Map.Entry;

public class JavaProperties {
	
	private static int MAX_KEY_LENGTH = 30;
	
	public static void main(String[] args) {
		
		System.out.println("Print all Java Properties.. Check them out. There is one called >>file.encoding<<!. Important One");
		Properties prop =  System.getProperties();
		Set<Entry<Object,Object>> set = prop.entrySet();
		
		for(Entry<Object,Object> entry : prop.entrySet()) {
			System.out.println(withPadding((String)entry.getKey()) + ":   " + entry.getValue());
		}
		
	}
	
	public static String withPadding(String source) {
		
		int sourceLen = source.length();
		String empty = " ";
		int difference = MAX_KEY_LENGTH - sourceLen;
		
		StringBuilder sb = new StringBuilder();
		sb.append(source);
		for(int i = 0 ; i <= difference ; i++) {
			sb.append(empty);
		}
		
		return sb.toString();
		
		
	}
	
	
	
	
}
