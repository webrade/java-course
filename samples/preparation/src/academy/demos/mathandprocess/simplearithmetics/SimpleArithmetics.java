package academy.demos.mathandprocess.simplearithmetics;

import academy.test.AcademyTest;
import academy.test.AcademyTestExecutor;

public class SimpleArithmetics {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		AcademyTest[] tests = { new AcademyTest("1+2+3-2-3+5-7+9-2-3", 3),
				new AcademyTest("1+2+3", 6), new AcademyTest("2+5+8", 15),
				new AcademyTest("8+8+3", 19), new AcademyTest("9+9-1-1+2", 18),
				new AcademyTest("3+3+2+2", 10), new AcademyTest("9+1-8", 2)};

		for(AcademyTest test : tests) {
			
			int expected = (int)test.getExpectedResult();
			int returned = calculateExpression((String)test.getSource());
			
			AcademyTestExecutor.execute(test, expected, returned);
			
		}
		
	}

	private static int calculateExpression(String source) {
		int result = Integer.valueOf("" + source.charAt(0));

		for (int i = 1; i < source.length(); i++) {

			if (source.charAt(i) == '+') {
				result += Integer.valueOf("" + source.charAt(i + 1));
				i++;
			} else if (source.charAt(i) == '-') {
				result -= Integer.valueOf("" + source.charAt(i + 1));
				i++;
			}

		}
		
		return result;
	}

}
