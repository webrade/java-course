package academy.demos.mathandprocess.primes;

import java.util.ArrayList;
import java.util.List;

public class FindPrimes {

	public static void main(String[] args) {
		
		int startNumber = 5500;
		int endNumber = 200000;
		List<Integer> primes = findInRange(startNumber, endNumber);
		
		System.out.println(primes);
		
	}

	private static List<Integer> findInRange(int startNumber, int endNumber) {
		List<Integer> result = new ArrayList<Integer>();
		for(int start = startNumber; start <= endNumber; start++) {
			if(isPrimeNumber(start)) {
				result.add(start);
			}
		}
		
		
		return result;
	}
	
	private static boolean isPrimeNumber(int number) {
		
		for(int start = 2; start < number; start ++) {
			
			if(number % start == 0) {
				return false;
			}
		}
		
		return true;
	}

}
