package academy.demos.strings.romannumbers;

import java.util.Arrays;

import academy.test.AcademyTest;
import academy.test.AcademyTestExecutor;

public class CalculateRomanNumbers {

	public static void main(String[] args) {
		
		System.out.println(">>Given Roman Numbers are as follows<<");
		System.out.println(Arrays.toString(getAllNumbers()));
		System.out.println("---Start converting different numbers---");
		
		AcademyTest[] tests = {
					new AcademyTest("I", 1), 
					new AcademyTest("XII", 12), 
					new AcademyTest("XIV", 14), 
					new AcademyTest("XIX", 19), 
					new AcademyTest("XXIV", 24), 
					new AcademyTest("L", 50), 
					new AcademyTest("LIII", 53), 
					new AcademyTest("LXXI", 71), 
					new AcademyTest("LXXXVIII", 88),
					new AcademyTest("LXXXIV", 84), 
					new AcademyTest("XXXIII", 33),
					new AcademyTest("CLXXXVIII", 188),
					new AcademyTest("CLXXVII", 177),
					new AcademyTest("CXIV", 114),
					new AcademyTest("CI", 101),
				};
		
		for(AcademyTest test : tests) {
			
			int expected = (int)test.getExpectedResult();
			int returned = convertToIntValue((String)test.getSource());
			
			AcademyTestExecutor.execute(test, expected, returned);
			
		}
		
		
	}
	
	/**
	 * That method is responsible for converting String roman number to Int value
	 * e.g. XIII  => 13 , XV = 15.
	 * We are expecting valid roman numbers, so won't do validation.
	 */
	private static int convertToIntValue(String romanNumber) {
		
		RomanNumber[] allGivenNumbers = getAllNumbers();
		
		// error condition we cannot continue our conversion.
		if(romanNumber == null || romanNumber.isEmpty()) {
			throw new IllegalArgumentException("The provided roman number is invalid");
		}
		
		// we have only 1 symbol, so just find the equivalent
		if(romanNumber.length() == 1) {
			char romanKey = romanNumber.charAt(0);
			return getRomanNumberByKey(romanKey, allGivenNumbers).getIntValue();
		}
		
		// assuming the roman number is given and it is complex ... start....
		int finalIntValue = 0;
		
		// start from left to right until the end
		// example number could be XIX => 19
		for(int i = 0 ; i < romanNumber.length(); i++) {
			
			char currentRoman = romanNumber.charAt(i);
			int currentRomanInt = getRomanNumberByKey(currentRoman, allGivenNumbers).getIntValue();
			
			// are we at the last number
			if(i == (romanNumber.length() - 1)) {
				finalIntValue += currentRomanInt;
				break;
			}
			
			char nextRoman = romanNumber.charAt(i+1);
			int nextRomanInt = getRomanNumberByKey(nextRoman, allGivenNumbers).getIntValue();
			
			// if current number is less than the next one. that means 
			// we have to substract and append the result to the total
			// after that we have to move the index 1 more position ++
			if(currentRomanInt < nextRomanInt) {
				// add the substraction to final
				finalIntValue = finalIntValue + nextRomanInt - currentRomanInt;
				// move the index one more position
				i++;
			} else {
				// if current number is bigger than the next one (or equal),
				// we can just add it to the sum
				finalIntValue += currentRomanInt;
			}
			
		}
		
		return finalIntValue;
		
		
	}
	
	private static RomanNumber getRomanNumberByKey(char key, RomanNumber[] numbers) {
		
		for(RomanNumber num : numbers) {
			if(num.getRomanKey() == key) {
				return num;
			}
		}
		
		throw new NullPointerException(String.format("Cannot find that number '%s' from the given list of numbers '%s'", key, Arrays.toString(numbers)));
		
	}
	
	
	/**
	 * Get all given roman numbers.
	 * Populate array and return that array with 
	 * all available numbers
	 * @return
	 */
	private static RomanNumber[] getAllNumbers() {
		
		RomanNumber[] allNumbers = new RomanNumber[5];
		
		RomanNumber num = new RomanNumber('I', 1);
		allNumbers[0] = num;
		
		num = new RomanNumber('V', 5);
		allNumbers[1] = num;
		
		num = new RomanNumber('X', 10);
		allNumbers[2] = num;
		
		num = new RomanNumber('L', 50);
		allNumbers[3] = num;
		
		num = new RomanNumber('C', 100);
		allNumbers[4] = num;
		
		return allNumbers;
	}

}
