package academy.demos.strings.romannumbers;

import java.util.Arrays;
import java.util.Scanner;

import academy.test.AcademyTest;
import academy.test.AcademyTestExecutor;

public class CalculateRomanNumbersSwitchCase {
	
	public static int sumDecimal (int decimal , int lastDesimal , int decimalNumber){
		if (lastDesimal > decimal) {
			return decimalNumber - decimal;
		}else{
			return decimalNumber + decimal;
		}
	}
	
	public static int convertRomanNumberToDecimalNumber(String romanNumber){
		int decimal = 0;
		int lastDecimal = 0;
		
		for (int i = romanNumber.length()-1; i >= 0; i--) {
			char romanIndex = romanNumber.charAt(i);
			
			switch(romanIndex){
			
			case 'M':
				decimal = sumDecimal(1000,lastDecimal,decimal);
				lastDecimal = 1000;
				break;
			case 'D':
				decimal = sumDecimal(500, lastDecimal, decimal);
				lastDecimal = 500;
				break;
			case 'C':
				decimal = sumDecimal(100, lastDecimal, decimal);
				lastDecimal = 100;
				break;
			case 'L':
				decimal = sumDecimal(50, lastDecimal, decimal);
				lastDecimal = 50;
				break;
			case 'X':
				decimal = sumDecimal(10, lastDecimal, decimal);
				lastDecimal = 10;
				break;
			case 'V':
				decimal = sumDecimal(5, lastDecimal, decimal);
				lastDecimal = 5;
				break;
			case 'I':
				decimal = sumDecimal(1, lastDecimal, decimal);
				lastDecimal = 1;
				break;
				
			}
		}
		
		return decimal;
	}

	public static void main(String[] args) {
		
		AcademyTest[] tests = {
					new AcademyTest("I", 1), 
					new AcademyTest("XII", 12), 
					new AcademyTest("XIV", 14), 
					new AcademyTest("XIX", 19), 
					new AcademyTest("XXIV", 24), 
					new AcademyTest("L", 50), 
					new AcademyTest("LIII", 53), 
					new AcademyTest("LXXI", 71), 
					new AcademyTest("LXXXVIII", 88),
					new AcademyTest("LXXXIV", 84), 
					new AcademyTest("XXXIII", 33),
					new AcademyTest("CLXXXVIII", 188),
					new AcademyTest("CLXXVII", 177),
					new AcademyTest("CXIV", 114),
					new AcademyTest("CI", 101),
				};
		
		for(AcademyTest test : tests) {
			
			int expected = (int)test.getExpectedResult();
			int returned = convertRomanNumberToDecimalNumber((String)test.getSource());
			
			AcademyTestExecutor.execute(test, expected, returned);
			
		}

	}

}