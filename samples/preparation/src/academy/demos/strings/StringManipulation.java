package academy.demos.strings;

public class StringManipulation {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		// for testing ...  call the methods here 
	}

	/**
	 * Exchange string halfs using substring
	 */
	public static String exchangeHalfs(String source) {
		
		String result = "";
		
		int half = source.length()/2;
		
		result = source.substring(half) + source.substring(0, half);
		
		return result;
		
	}

	/**
	 * Exchange string halfs using loop
	 */
	public static String exchangeHalfsWithLoop(String source) {
		
		String result = "";
		
		int half = source.length() / 2;
		
		// start the loop from the half index
		for(int index = half; index < source.length(); /** nothing here */) {

			result += source.charAt(index);
			
			// we increment here
			index++;
			
			// reset the index to 0 once we reach the end
			if(index == source.length()) {
				index = 0;
			}
			
			// if we reach where we started, stop the loop
			if(index == half) {
				break;
			}
		}
		
		return result;
		
	}
	
}
