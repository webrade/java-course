package academy.demos.designpatterns.observer;

public class LowerCaseNameObserver extends Observer {

	public LowerCaseNameObserver(Model m) {
		super(m);
	}

	@Override
	public void observe() {
		System.out.println(getModel().getName().toLowerCase());
	}
		
}
