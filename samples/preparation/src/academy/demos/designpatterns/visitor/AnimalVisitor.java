package academy.demos.designpatterns.visitor;

public interface AnimalVisitor {

	void visit(Cat cat);
	void visit(Dog dog);
}
