package academy.demos.designpatterns.visitor;

public interface Animal {
	
	public void accept(AnimalVisitor animal);
}
