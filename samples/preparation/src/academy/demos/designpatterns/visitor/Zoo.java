package academy.demos.designpatterns.visitor;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Zoo {
 	
	Set<Animal> animals = new HashSet<>(Arrays.asList(new Cat(), new Dog()));
	
	public void accept(AnimalVisitor visitor) {
		for(Animal a : animals) {
			a.accept(visitor);
		}
	}
}
