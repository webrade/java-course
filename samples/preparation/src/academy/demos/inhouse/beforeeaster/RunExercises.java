package academy.demos.inhouse.beforeeaster;

/**
 * That class will test all exercises written
 * @author radkol
 *
 */
public class RunExercises {

	public static void main(String[] args) {
		
		// task 1
		String testTransformString = Exercises.transformString("ABCDEF");
		System.out.println(testTransformString);
		
		testTransformString = Exercises.transformString("RADKO");
		System.out.println(testTransformString);
		
		// task 2
		String[] singers = new String[]{
			 "Beyonce (f)",
			 "David Bowie Junior (m)",
			 "Madonna (f)",
			 "Madonna Junior ",
			 "Elton John (m)"	
		};
		System.out.println("-------------------");
		Exercises.countSingers(singers);
		
		
		// task 3
		String[] instruments = new String[]{
				 "cello",
				 "guitar",
				 "violin",
				 "double bass",
			};
		System.out.println("-------------------");
		Exercises.removeVowels(instruments, "[aueyio]");
		
		// task 4
		System.out.println("-------------------");
		System.out.println(Exercises.countNumbers(15));
		
		// task 5
		System.out.println("-------------------");
		Exercises.generateCheckerBoard();
		
		// task 6
		System.out.println("-------------------");
		Exercises.printDartsValues();
		
		// task 7
		System.out.println("-------------------");
		Exercises.printWordDiagonally("Academy");
		
		// task 8
		System.out.println("---------------");
		System.out.println(Exercises.isSeriesConsecutive("8-7-6-5-4"));
		System.out.println(Exercises.isSeriesConsecutive("1-2-3-4-5"));
		System.out.println(Exercises.isSeriesConsecutive("1-1-3-4-5"));
		System.out.println(Exercises.isSeriesConsecutive("1-6-3-4-5"));
		System.out.println(Exercises.isSeriesConsecutive("1-2-1-2-3"));
		
		// taks 9
		String[] source = new String[]{
			"Manchester United 1 Chelsea 0",
			"Arsenal 1 Manchester United 1",
			"Manchester United 3 Fulham 1",
			"Liverpool 2 Manchester United 1",
			"Swansea 2 Manchester United 4"
		};
		Exercises.showTeamStatistic("Manchester United", source);
	}

}
