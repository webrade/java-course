package academy.demos.database.complex;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Make sure we load the file which has our configuration details
 * so we can connect to the database.
 * @author radkol
 *
 */
public class ConfigurationLoader {

	private static final String CONFIG_FILE_LOCATION = "database/config.properties";
	private static final String CONFIG_FILE_DELIMITER = "=";
	
	private static final Map<String,String> items = new HashMap<>();
	
	static {
		try {
			BufferedReader reader = new BufferedReader(new FileReader(CONFIG_FILE_LOCATION));
			String line = null;
			while ((line = reader.readLine()) != null) {
				String[] parts = line.split("[" + CONFIG_FILE_DELIMITER + "]");
				
				if(parts.length < 1) {
					throw new RuntimeException("Your configuration is not correct. Cannot process " + line);
				}
				
				if(parts.length == 1) {
					items.put(parts[0], "");
				} else {
					items.put(parts[0], parts[1]);
				}
			}
			reader.close();
		} catch (IOException e) {}
	}
	
	public static String get(String key) {
		return items.get(key);
	}
	
}
