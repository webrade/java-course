package academy.demos.clockdegrees;

import java.util.Scanner;

public class ClockDegreesCalculator {

	private static final Integer HOURS_FROM = 0;
	private static final Integer HOURS_TO = 23;
	private static final Integer MINUTE_FROM = 0;
	private static final Integer MINUTE_TO = 59;

	static class InputHandler {
		
		private Scanner input;
		
		public InputHandler(Scanner input) {
			this.input = input;
		}
		
		public void start() {
			System.out.println("---enter data: ---- \n");
		}
		
		public int getMinutes() {
			System.out.println("Input minutes 0 - 59");
			while (true) {
				String line = input.nextLine();
				if (validMinute(line)) {
					return Integer.parseInt(line);
				}
				System.out.println("Invalid minute value. Insert value between 0 - 59 \n");
			}
		}
		
		public int getHours() {
			System.out.println("Input hours 0 - 23");
			while (true) {
				String line = input.nextLine();
				if (validHour(line)) {
					return Integer.parseInt(line);
				}
				System.out.println("Invalid hour value. Insert value between 0 - 23 \n");
			}
		}
		
		public void end() {
			System.out.println("----Calculated !----");
			this.input.close();
		}
		
		private boolean validHour(String input) {

			try {
				int toInt = Integer.parseInt(input);
				return toInt >= HOURS_FROM && toInt <= HOURS_TO;
			} catch (Exception e) {
				return false;
			}
		}

		private boolean validMinute(String input) {
			try {
				int toInt = Integer.parseInt(input);
				return toInt >= MINUTE_FROM && toInt <= MINUTE_TO;
			} catch (Exception e) {
				return false;
			}
		}
	}

	static class Calculator {

		private static final int SINGLE_DISTANCE = 6;
		private static final int SINGLE_DISTANCE_PER_GROUP = 5;
		private static final int FULL_DISTANCE = 360;
		
		private final int hours;
		private final int minutes;

		public Calculator(int hours, int minutes) {
			this.hours = hours;
			this.minutes = minutes;
		}

		public int calculate() {
			return Math.abs((hours == HOURS_FROM ? FULL_DISTANCE : hours * SINGLE_DISTANCE * SINGLE_DISTANCE_PER_GROUP)
					- (minutes == MINUTE_FROM ? FULL_DISTANCE : minutes * SINGLE_DISTANCE));
		}

	}

	public static void main(String[] args) {
		InputHandler inputHandler = new InputHandler(new Scanner(System.in));
		inputHandler.start();
		int hours = inputHandler.getHours();
		int minutes = inputHandler.getMinutes();
		Calculator calculator = new Calculator(hours, minutes);
		System.out.println(String.format("The degree between %d : %d is %d degrees", hours, minutes,
				calculator.calculate()));
		inputHandler.end();
	}

}
