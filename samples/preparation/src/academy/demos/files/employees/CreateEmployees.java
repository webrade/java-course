package academy.demos.files.employees;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;


public class CreateEmployees {

	
	public static void main(String[] args) {
		
		String filePath = "files/employees.txt";
		File employeeFile = new File(filePath);
		Scanner myStream = null;
		
		
		Employee[] myEmployees = null;
		
		int currentLineIndex = 0;
		int arrayIndex = 0;
		try {
			myStream = new Scanner(employeeFile);
			
			while(myStream.hasNextLine()) {
				String currentLine = myStream.nextLine();
				currentLineIndex++;
				if(currentLineIndex == 1) {
					int size = Integer.valueOf(currentLine);
					myEmployees = new Employee[size];
					continue;
				}
				String[] lineParts = currentLine.split("[;]");
				
				System.out.println(Arrays.toString(lineParts));
				Employee currentEmp = new Employee();
				currentEmp.setName(lineParts[0]);
				currentEmp.setAge(Integer.valueOf(lineParts[1]));
				currentEmp.setSalary(Integer.valueOf(lineParts[2]));
				currentEmp.setProfession(lineParts[3]);
				
				myEmployees[arrayIndex] = currentEmp;
				arrayIndex++;
			}
			
			
		} catch (FileNotFoundException e) {
			System.out.println("File is not found");
		} finally {
			if(myStream != null) {
				myStream.close();
			}
		}
		
		// you can use Arrays.toString(anArray); to print an array.
		
		System.out.println(Arrays.toString(myEmployees));
		
		// OR
		
//		for(Employee cEmployee : myEmployees) {
//			System.out.println(cEmployee);
//		}
		
	}
}
