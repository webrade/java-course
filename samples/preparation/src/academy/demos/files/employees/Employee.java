package academy.demos.files.employees;

public class Employee {

	private String name;
	private int age;
	private int salary;
	private String profession;
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String newName) {
		this.name = newName;
	}
	
	public int getAge(){
		return this.age;
	}
	
	public int getSalary() {
		return salary;
	}
	
	public String getProfession() {
		return profession;
	}
	
	public void setAge(int newAge) {
		this.age = newAge;
	}
	
	public void setProfession(String newProfession) {
		this.profession = newProfession;
	}
	
	public void setSalary(int newSalary) {
		this.salary = newSalary;
	}
	
	@Override
	public String toString() {
		return String.format("My name is %s, I'm %d old, "
				+ "I earn %d and I'm working in %s deparment",
				this.name, this.age, this.salary, this.profession);
	}
	
}
