package edu.jete.lecture1;

import edu.jete.lecture1.help.Help;

/**
 * This class is the initial point of the application.
 * When started, java is trying to load the class with
 * main method, and this class has it, so it will execute
 * 
 * @author radko
 *
 */
public class Run {
	
	public static void main(String[] args) {
	
		
		System.out.println("Hello Run Class, I need Help so I will try to call it.");
		new Help().doHelp();
		
		System.out.println(factorialRecursive(2));
		System.out.println(factorialRecursive(3));
		System.out.println(factorialRecursive(4));
		System.out.println(factorialRecursive(5));
		System.out.println(factorialRecursive(6));
		System.out.println(factorialRecursive(7));
		System.out.println(factorialRecursive(8));
		System.out.println(factorialRecursive(9));
		System.out.println(factorialRecursive(10));
		
	}
		
	
	public static int factorialRecursive(int source) {
		if( source == 1) return source;
		return source * factorialRecursive(source - 1);
	}
	
	/**
	 * This method will calculate the factorial for input number.
	 */
	public static int factorialIterative(int source) {
		int result = 1;
		for(int currentNumber = 1 ; currentNumber <= source ; currentNumber = currentNumber + 1) {
			result = result * currentNumber;
		}
		
		return result;
	}
	
}
