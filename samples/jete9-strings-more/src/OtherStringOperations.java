public class OtherStringOperations {

	public static void main(String[] args) {
		// String.replace() example
		String cocktail = "Vodka + Martini + Cherry";
		String replaced = cocktail.replace("+", "and");
		System.out.println(replaced);

		// Uppercase and lowercase conversion examples
		String alpha = "aBcDeFg";
		String lowerAlpha = alpha.toLowerCase();
		System.out.println(lowerAlpha);
		String upperAlpha = alpha.toUpperCase();
		System.out.println(upperAlpha);

		// trim() example
		String s = "  example of white space ";
		String clean = s.trim();
		System.out.println(clean);
	}

}
