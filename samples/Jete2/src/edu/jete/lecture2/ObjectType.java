package edu.jete.lecture2;

public class ObjectType extends Object{

	public static void main(String[] args) {
		Object dataContainer = 5;
		System.out.print("The value of dataContainer is: ");
		System.out.println(dataContainer);
		dataContainer = "Five";
		System.out.print("The value of dataContainer is: ");
		System.out.println(dataContainer);
	}

}
