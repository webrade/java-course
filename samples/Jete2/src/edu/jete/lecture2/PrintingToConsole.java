package edu.jete.lecture2;
import java.time.LocalDate;
import java.util.Locale;

public class PrintingToConsole {
	public static void main(String[] args) {
		Locale.setDefault(Locale.ROOT); 
		
		String name = "Plovdiv Academy";
		String location = "Plovdiv";
		double age = 0.5d;
		System.out.print(name);
		System.out.println(" is " + age +
			" years old organization located in " +
			location + ".");
		
		// formatted printing
		
		System.out.printf(
			"%s is %.2f years old organization located in %s.\n",
			name, age, location);
		
		System.out.printf("Today is %1$td.%1$tm.%1$tY\n",
				LocalDate.now());
		
		System.out.printf("%1$d + %1$d = %2$d", 2, 4);
	}
}
