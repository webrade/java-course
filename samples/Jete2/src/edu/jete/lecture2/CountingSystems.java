package edu.jete.lecture2;

public class CountingSystems {

	public static void main(String[] args) {
		
		// десетично представяне
		int base10 = 100;
		
		// двоично представяне
		int base2 = 0b1100100;
		
		// осмично представяне
		int base8 = 0144;
		
		// шестайсетично представяне
		int base16 = 0x64;
		
		System.out.println(base10 == base2);
		System.out.println(base10 == base8);
		System.out.println(base10 == base16);
		

	}

}
