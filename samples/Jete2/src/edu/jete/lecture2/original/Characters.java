package edu.jete.lecture2.original;

public class Characters {

	public static void main(String[] args) {
		char symbol = 'a';
		System.out.println("The code of '" + symbol + "' is: " + (int) symbol);

		symbol = 'b';
		System.out.println("The code of '" + symbol + "' is: " + (int) symbol);

		symbol = 'A';
		System.out.println("The code of '" + symbol + "' is: " + (int) symbol);
	}
}
