package jete.lecture3.strings;

public class StringBuilderDemo {
	public static String reverseIt(String s) {
		StringBuilder sb = new StringBuilder();
		for (int i = s.length() - 1; i >= 0; i--)
			sb.append(s.charAt(i));
		return sb.toString();
	}

	public static String extractCapitals(String s) {
		StringBuilder result = new StringBuilder();
		for (int i = 0; i < s.length(); i++) {
			char ch = s.charAt(i);
			if (Character.isUpperCase(ch)) {
				result.append(ch);
			}
		}
		return result.toString();
	}

	public static void main(String[] args) {
		String s = "Plovdiv Academy for Java Development";

		String reversed = reverseIt(s);
		System.out.println(reversed);

		String capitals = extractCapitals(s);
		System.out.println(capitals);
	}

}
