package jete.lecture3;

public class StringType {
	public static void main(String[] args) {
		
		Integer test = 100;
		
		String firstName = "Radko11222";
		String lastName = "Lyutskanov";
		System.out.println("Hello, " + firstName);

		String fullName = firstName + " " + lastName;
		System.out.println("Your full name is: " + fullName);
		
		int age = 27;
		System.out.println("Hello, I am " + age + " years old");
		
		
		
		/// Formatted String
		
//		System.out.println("------");
//		
//		System.out.printf("My full name is %s, Hello, I'm %d years old.", fullName, age);
	}
}
