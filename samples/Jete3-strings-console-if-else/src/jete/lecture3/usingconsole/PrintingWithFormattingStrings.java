package jete.lecture3.usingconsole;

public class PrintingWithFormattingStrings {

	public static void main(String[] args) {
		String str = "fdhdfhnfbyfy\t";
		System.out.printf("%s", str);
		System.out.println();

		String name = "Peter";
		int age = 18;
		String town = "Plovdiv";
		
		String all = name + " is " + age + " years old from " + town;
		
		System.out.println(all);
		System.out.printf("%s is %d years old from %s.\n",
			name, age, town);
		System.out.printf(
			"%1$s is big town.\n" +
			"%1$s is the capital of Bulgaria.\n" +
			"%2$s lives in %1$s.\n" +
			"%2$s is %3$d years old.\n",
			town, name, age);
		System.out.printf("The time is: %1$tH:%1$tM:%1$tS.\n",
			new java.util.Date());
		
		int a = 2, b = 3;
		System.out.printf("%d + %d =", a, b);
		System.out.printf(" %d\n", (a + b));
		// 2 + 3 = 5
		System.out.printf("%d * %d = %d%n", a, b, a * b);
		// 2 * 3 = 6
		float pi = 3.14159206f;
		System.out.printf("%.2f%n", pi);
		// 3,14
		System.out.printf("%.5f%n", pi); 
		// 3,14159
		
		double colaPrice = 1.20;
		String cola = "Coca Cola";
		double fantaPrice = 1.20;
		String fanta = "Fanta Dizzy";
		double zagorkaPrice = 1.50;
		String zagorka = "Zagorka";

		System.out.println("\nMenu:");
		System.out.printf("1. %s %.2f%n", 
		  cola, colaPrice);
		System.out.printf("2. %s %.2f%n", 
		  fanta, fantaPrice);
		System.out.printf("3. %s %.2f%n", 
		  zagorka, zagorkaPrice);
		System.out.println();
		
		System.out.printf("%s is %d years old from %s.", name, age, town);
		System.out.print("This is on the same line!");

		System.out.println("Next sentence will be" +
			" on a new line.");

		System.out.printf("Bye, bye, %s from %s.%n", name, town);
		
		
	}

}
