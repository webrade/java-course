package jete.lecture3.usingconsole;

public class SimplePrinting {

	public static void main(String[] args) {
		int i = 15;
		System.out.print(i);
		// 15

		System.out.println();

		double a = 15.5;
		int b = 14;
		System.out.print(a + " + " + b + " = " + (a + b));
		// 15.5 + 14 = 29.5
		
		System.out.println();
		
		String str = "Hello Java!";
		System.out.println(str);
		// Hello Java!
		
		String name = "Radko";
		int year = 1988;
		System.out.println(name + " was born in " + year + ".");
		// Radko was born in 1988.
	}

}
