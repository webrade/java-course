package jete.lecture3.conditionalstatements;
import java.util.Scanner;

public class IfStatementExample {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Please enter two numbers (on separate lines).");

		int firstNumber = input.nextInt();
		int secondNumber = input.nextInt();

		int biggerNumber = firstNumber;
		if (secondNumber > firstNumber) {
			biggerNumber = secondNumber;
		}

		System.out.printf("The bigger number is: %d%n", biggerNumber);
	}

}
