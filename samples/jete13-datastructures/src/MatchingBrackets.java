import java.util.Stack;

public class MatchingBrackets {

	public static void main(String[] args) {
		String expression = "1 + (3 + 2 - (2+3) * 4 - ((3+1)*(4-2)))";
		Stack<Integer> stack = new Stack<Integer>();

		for (int index = 0; index < expression.length(); index++) {
			char ch = expression.charAt(index);
			if (ch == '(') {
				stack.push(index);
			} else if (ch == ')') {
				int startIndex = (int) stack.pop();
				String contents = expression.substring(startIndex, index + 1);
				System.out.println(contents);
			}
		}
	}

}
