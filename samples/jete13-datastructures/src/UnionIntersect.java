import java.util.ArrayList;
import java.util.Arrays;

public class UnionIntersect {

	public static Integer[] union(Integer[] firstArr, Integer[] secondArr) {
		ArrayList<Integer> union = new ArrayList<Integer>();
		for (Integer item : firstArr) {
			union.add(item);
		}
		for (Integer item : secondArr) {
			if (!union.contains(item)) {
				union.add(item);
			}
		}
		return union.toArray(new Integer[union.size()]);
	}

	public static Integer[] intersect(Integer[] firstArr, Integer[] secondArr) {
		ArrayList<Integer> intersect = new ArrayList<Integer>();
		for (Integer item : firstArr) {
			if (Arrays.binarySearch(secondArr, item) >= 0) {
				intersect.add(item);
			}
		}
		return intersect.toArray(new Integer[intersect.size()]);

	}

	public static void printArray(Integer[] arr) {
		System.out.print("{");
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i]);
			if (i < arr.length - 1) {
				System.out.print(", ");
			}
		}
		System.out.println("}");
	}

	public static void main(String[] args) {
		Integer[] firstArr = { 1, 2, 3, 4, 5 };
		System.out.print("firstArr = ");
		printArray(firstArr);

		Integer[] secondArr = { 2, 4, 6 };
		System.out.print("secondArr = ");
		printArray(secondArr);

		Integer[] unionArr = union(firstArr, secondArr);
		System.out.print("union = ");
		printArray(unionArr);

		Integer[] intersectArr = intersect(firstArr, secondArr);
		System.out.print("intersect = ");
		printArray(intersectArr);
	}

}
