import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class HashtableExample {

	public static void main(String[] args) {
		HashMap<String, Integer> studentsMarks = new HashMap<String, Integer>();
		studentsMarks.put("Ivan", 4);
		studentsMarks.put("Peter", 6);
		studentsMarks.put("Maria", 6);
		studentsMarks.put("George", 5);

		int peterMark = studentsMarks.get("Peter");
		System.out.println("Peter's mark: " + peterMark);

		studentsMarks.remove("Peter");
		System.out.println("Peter removed.");

		System.out.println("Is Peter in the hash table: "
				+ studentsMarks.containsKey("Peter"));

		studentsMarks.put("Milena", 5);
		System.out.println("Added Milena.");

		studentsMarks.put("Ivan", 3);

		System.out.println("Ivan's mark changed.");

		
		// Print all elements of the hash table
		System.out.println("Students and grades:");
		for (Map.Entry<String, Integer> studentMark : studentsMarks.entrySet()) {
			System.out.printf("%s --> %d%n", studentMark.getKey(), studentMark
					.getValue());
		}
		
		System.out.println("------------");
		Collection<String> studentMarksKeys  = studentsMarks.keySet();
		for(String name : studentMarksKeys) {
			System.out.printf("%s --> %d%n", name, studentsMarks.get(name));
		}

	}

}
