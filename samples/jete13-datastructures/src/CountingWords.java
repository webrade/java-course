import java.util.HashMap;

public class CountingWords {

	public static void main(String[] args) {
		String s = "Welcome to our Java course. In this "
				+ "course you will learn how to write simple "
				+ "programs in Java";
		String[] words = s.split("[ ,.]");
		HashMap<String, Integer> wordsCount = new HashMap<String, Integer>();
		for (String word : words) {
			if (!"".equalsIgnoreCase(word)) {
				int count = 1;
				if (wordsCount.containsKey(word)) {
					count += wordsCount.get(word);
				}
				wordsCount.put(word, count);
			}
		}
		for (String word : wordsCount.keySet()) {
			System.out.printf("%s --> %d%n", word, wordsCount.get(word));
		}
	}
}
