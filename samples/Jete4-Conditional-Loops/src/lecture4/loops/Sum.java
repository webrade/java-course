package lecture4.loops;

import java.util.Scanner;

public class Sum {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.print("n = ");
		int n = input.nextInt();
		int num = 1;
		int sum = 1;

		System.out.print("The sum 1");
		while (num < n) {
			num = num + 3;
			sum += num;
			System.out.printf("+" + num);
		}
		System.out.printf(" = " + sum);
	}
	
}
