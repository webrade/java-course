package lecture4.conditionallogic;

import java.util.Scanner;

public class IfElseStatement {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.print("Please enter a number: ");
		double number = input.nextDouble();

		
		System.out.println(number % 2);
		if (number % 2 == 0) {
			System.out.println("This number is even.");
		} else {
			System.out.println("This number is odd.");
		}
		
	}

}
