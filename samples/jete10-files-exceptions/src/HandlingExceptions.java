import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class HandlingExceptions {

	public static void main(String[] args) {
		String fileName = "files/test1.txt";
		Scanner fileInput = null;
		try {
			fileInput = new Scanner(new File(fileName), "windows-1251");
			System.out.printf("File %s successfully opened.%n", fileName);
			System.out.println("File contents:");
			while (fileInput.hasNextLine()) {
				System.out.println(fileInput.nextLine());
			}
		} catch (NullPointerException npe) {
			// Never left this block empty!!!
			// This is very bad practice!
		} catch (FileNotFoundException fnf) {
			System.err.printf("Can not find file %s.", fileName);
		} finally {
			if (null != fileInput) {
				fileInput.close();
			}
		}
	}
}
