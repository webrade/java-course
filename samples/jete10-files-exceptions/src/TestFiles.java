import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class TestFiles {

	public static void main(String[] args) throws FileNotFoundException{
		
		File myFile = new File("test11111.txt");
		
		Scanner fileReader = null;
		try {
			fileReader = new Scanner(myFile);
			while(fileReader.hasNextLine()) {
				String currentLine = fileReader.nextLine();
				System.out.println(currentLine);
			}
		} finally {
			System.out.println("This will be executed");
		}

		
		
	}

}
