package jete.solution;

import static org.junit.Assert.*;

import java.lang.reflect.Method;

import jete.AbstractTest;
import jete.helpers.Student;

import org.junit.Before;
import org.junit.Test;

public class TaskSolutionTest extends AbstractTest {
	
	@Before
	public void setup() {
		super.setup("jete.solution", "TaskSolution");
	}
	
	@Test
	public void readNumbers() {
		
		TaskSolution examineObject = (TaskSolution)createInstance(getClass(this.getPackageName(), this.getExamineClass()));
		executeTest(examineObject, "readNumbers", 39 * 40 / 2, new Object[0], new Class[0]);
		
	}
	
	@Test
	public void createStudents() {
		TaskSolution examineObject = (TaskSolution)createInstance(getClass(this.getPackageName(), this.getExamineClass()));
		
		Method m  = getMethod(examineObject.getClass(), "createStudents", new Class[0]);
		Object result = invokeMethod(m, examineObject, new Object[0]);
		
		assertTrue(String.format("Result array should not be null '%s' received", result), result != null);
		
		if(!(result instanceof Student[])) {
			assertTrue(String.format("Result array is not Student[] type, '%s' received", result), false);
		}
		
		Student[] students = (Student[])result;
		
		assertTrue(String.format("Result array size should be 5, '%d' received", students.length), students.length == 5);
		
		assertTrue(String.format("Name of the first student should be 'Todor', '%s' received", students[0].getName()), "Todor".equals(students[0].getName()));
		assertTrue(String.format("Phone of the second student should be '0787 000 000', '%s' received", students[1].getPhone()), "0787 000 000".equals(students[1].getPhone()));
		assertTrue(String.format("Age of the third student should be '19', '%d' received", students[2].getAge()), 19 == students[2].getAge());
		assertTrue(String.format("Course of the fifth student should be 'Scala(Java) Programming', '%s' received", students[4].getCourse()), "Scala(Java) Programming".equals(students[4].getCourse()));
	}
}
