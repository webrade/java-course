package jete.solution;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import jete.helpers.Student;
import jete.helpers.Utility;

public class TaskSolution {
	
	/**
	 * Task1
	 * Write a method 'readNumbers' which has no arguments and which
	 * reads the entire content of the file "numbers.txt", line by line and try to sum
	 * all numbers in every line. Note that numbers are separated by empty space and not all of them are valid numbers!.
	 * The method should return the total sum of all valid numbers in all lines.
	 * The method should return int value which is the calculated sum.
	 * Handle the exceptions by printing error to the console.
	 * Hint: You can use the Utility.java in helpers package to check if string can be converted to int
	 * if it is not valid int, you should not add it to the final sum.
	 */
	public int readNumbers() {
		Scanner input = null;
		try {
			input = new Scanner(new File("numbers.txt"));
			int sum = 0;
			while(input.hasNextLine()) {
				String currentLine = input.nextLine();
				String[] parts = currentLine.split("[ ]");
				for(String part : parts) {
					if(Utility.isValidInt(part)) {
						sum += Integer.parseInt(part);
					}
				}
			}
			
			return sum;
		} 
		catch (FileNotFoundException e) {
			System.out.println("File Can't be found. Actual Error is " + e.getMessage());
		} finally {
			if(input != null) {
				input.close();
			}
		}
		
		return 0;
	}
	
	
	/**
	 * Task 2
	 * Write a method 'createStudents' which has no arguments.
	 * The method should return Student[] -> array of Students objects. Student class is in helpers.Student package.
	 * The method should read the file "students.txt" line by line and:
	 * First line of the file will contain the number of lines(Student records) that exists.
	 * It has to ignore the second line of the file which has no student content and it is just a header of the file(like a table header).
	 * Then for every other line, it should process the line and build Student object with the information in the line.
	 * Make sure you trim the content properly!!! -> use trim() function
	 * What will be the character(s) that you have to split the line on?
	 * Create an array of students and add all Student objects that are created in that array.
	 * Return the array as a result from that method.
	 * 
	 */
	public Student[] createStudents() {
		
		Scanner input;
		try {
			input = new Scanner( new File("students.txt"));
		} catch (FileNotFoundException e) {
			System.out.println("File Can't be found. Actual Error is " + e.getMessage());
			return null;
		}
		
		// we can't create the array since we don't know the size
		Student[] result = null;
		
		int currentLineIndex = 0;
		int studentsArrayIndex = 0;
		while(input.hasNextLine()) {
			
			String currentLine = input.nextLine();
			currentLineIndex++;
			
			// now if we are on the first line ... create the Array which will hold all the students
			// and move on to the next lines.
			if(currentLineIndex == 1) {
				result = new Student[Integer.parseInt(currentLine)];
				continue;
			}
			
			// according to the exercise we have to "skip the second line"
			if(currentLineIndex == 2) {
				continue;
			}
			
			// now we can start creating Students
			result[studentsArrayIndex++] = createStudentFromLine(currentLine);
		}
		
		input.close();
		
		return result;
	}

	private Student createStudentFromLine(String currentLine) {
		
		String delimiter = "##";
		
		String[] parts = currentLine.split("##");
		
		// we should have 4 parts
		// name // age // phone // course
		Student student = new Student();
		student.setName(parts[0].trim());
		student.setAge(Utility.isValidInt(parts[1].trim()) ? Integer.parseInt(parts[1].trim()) : 0);
		student.setPhone(parts[2].trim());
		student.setCourse(parts[3].trim());
		
		return student;
	}
}
