package jete.helpers;

public class Utility {

	public static boolean isValidInt(String source) {
		try {
			Integer.parseInt(source);
			return true;
		} catch( NumberFormatException nfe) {
			System.out.println("Cannot parse number '" + source + "'");
		}
		return false;
	}
}
