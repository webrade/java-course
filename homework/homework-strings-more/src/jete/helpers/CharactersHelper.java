package jete.helpers;

public class CharactersHelper {

	private static char zeroDigit = '0';
	private static int oneDigit = '1';
	
	/**
	 * Check if character is valid digit
	 */
	public static boolean validDigit(char digitChar) {
		int digitAscii = (int)digitChar;
		if((digitAscii >= oneDigit && digitAscii < oneDigit + 9) || (digitChar == zeroDigit)) {
			return true;
		}
		return false;
	}
	
	/**
	 * Check if character is 0
	 */
	public static boolean isZero(char digitChar) {
		return digitChar == zeroDigit;
	}
	
	/**
	 * Check if character is 1
	 */
	public static boolean isOne(char digitChar) {
		return digitChar == oneDigit;
	}
	
	public static boolean isCharDigitLessThan(char sourceDigitChar, int lessThanInt) {
		
		if(!validDigit(sourceDigitChar)) {
			return false;
		}
		
		if(lessThanInt > 9 || lessThanInt < 0) {
			return false;
		}
		
		char limitAsChar = ("" + lessThanInt).charAt(0);
		
		if(limitAsChar > sourceDigitChar) {
			return true;
		}
		
		return false;
	}
	
	public static boolean isCharDigitEqualTo(char sourceDigitChar, int equalInt) {
		
		if(!validDigit(sourceDigitChar)) {
			return false;
		}
		
		if(equalInt > 9 || equalInt < 0) {
			return false;
		}
		
		char limitAsChar = ("" + equalInt).charAt(0);
		if(limitAsChar == sourceDigitChar) {
			return true;
		}
		
		return false;
	}
	
}
