package jete.solution;

/**
 * Abstract class animal. Parent of all
 * @author radkol
 *
 */
public abstract class Animal {
	
	/**
	 * Common for all animals right ???
	 */
	private int age;
	private GenderEnumeration gender;
	private String name;
	
	/**
	 * That method should be implemented in child classes right ?
	 * Different animal produce different sound, so abstract the sound method
	 */
	public abstract void produceSound();
	
	/**
	 * We can write the method here since all animals will display their info the same way.
	 */
	public String getMyDetails() {
		String details = String.format("Hello, I'm %s, My gender is %s, and I'm %d years old", this.getName(), this.getGender(), this.getAge());
		return details;
	}
	
	/**
	 * Age
	 */
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	
	/**
	 * Gender
	 */
	public GenderEnumeration getGender() {
		return gender;
	}
	
	public void setGender(GenderEnumeration gender) {
		this.gender = gender;
	}
	
	/**
	 * Name
	 */
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
	
	
}
