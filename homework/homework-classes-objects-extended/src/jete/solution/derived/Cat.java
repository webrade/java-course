package jete.solution.derived;

import jete.solution.Animal;

public abstract class Cat extends Animal {
	
	/**
	 * Override here? Why, because all kind of cats will say Miauuuu
	 */
	@Override
	public void produceSound() {
		System.out.printf("Cat %s is saying Miauuuu" , this.getName());
	}
	
}
