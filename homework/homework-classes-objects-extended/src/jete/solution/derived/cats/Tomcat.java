package jete.solution.derived.cats;

import jete.solution.GenderEnumeration;
import jete.solution.derived.Cat;

/**
 * This can be only male right ???
 * hence override set gender method and always set GenderEnumeration.MALE
 *
 */
public class Tomcat extends Cat{

	@Override
	public GenderEnumeration getGender() {
		return GenderEnumeration.MALE;
	}
	
}
