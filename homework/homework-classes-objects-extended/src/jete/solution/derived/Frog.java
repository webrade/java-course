package jete.solution.derived;

import jete.solution.Animal;

public class Frog extends Animal {

	@Override
	public void produceSound() {
		System.out.printf("Frog %s is saying Krqk - Krqk - Krqk" , this.getName());
	}

}
