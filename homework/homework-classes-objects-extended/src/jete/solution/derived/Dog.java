package jete.solution.derived;

import jete.solution.Animal;

public class Dog extends Animal {

	@Override
	public void produceSound() {
		System.out.printf("Dog %s is saying Bau - Bau - Bau" , this.getName());
	}

}
