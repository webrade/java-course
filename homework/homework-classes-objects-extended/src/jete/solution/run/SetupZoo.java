package jete.solution.run;

import jete.solution.Animal;
import jete.solution.GenderEnumeration;
import jete.solution.derived.Cat;
import jete.solution.derived.Dog;
import jete.solution.derived.Frog;
import jete.solution.derived.cats.Kitten;
import jete.solution.derived.cats.Tomcat;

public class SetupZoo {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		// create cats
		Animal kitten1 = new Kitten();
		kitten1.setAge(1);
		// doesn't matter what will be provided, so we can not set it
		kitten1.setGender(GenderEnumeration.FEMALE);
		kitten1.setName("Kitten>>1<<");
		
		Animal tomcat1 = new Tomcat();
		tomcat1.setAge(3);
		tomcat1.setName("Tomcat>>1<<");
		
		// we can't create Cat why ???
		// Cat cat = new Cat(); Compile Error
		
		Animal frog = new Frog();
		frog.setGender(GenderEnumeration.MALE);
		frog.setAge(30);
		frog.setName("Frog>>1<<");
		
		Animal dog = new Dog();
		dog.setGender(GenderEnumeration.FEMALE);
		dog.setAge(11);
		dog.setName("Dog>>1<<");
		
		// create array of animals 
		
		Animal[] zoo = new Animal[4];
		zoo[0] = kitten1;
		zoo[1] = tomcat1;
		zoo[2] = frog;
		zoo[3] = dog;
		
		for(Animal animal : zoo) {
			System.out.println("--- Start Animal ---");
			System.out.println("--- Sound ---");
			animal.produceSound();
			System.out.println("\n--- Details ---");
			System.out.println(animal.getMyDetails());
			System.out.println("--- End Animal --- \n");
		}
		
	}
	

}
