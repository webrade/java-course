package jete.lecture5;

import static org.junit.Assert.assertTrue;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class AbstractTest {
	
	protected static final String messageFormat = "%s:Expected result is '%s', actual result is '%s' ...";
	protected static final String messageException = ">> There was an error while running the test for %s method. Please skip this one for now. Exception message: %s <<";
	protected static final String messageNoMethod = "There is no static method with name '%s'. Please create it!";
	@SuppressWarnings("unchecked")
	protected Method getMethod(@SuppressWarnings("rawtypes") Class cls, String methodName) {
		
		try {
			Method method = cls.getMethod(methodName);
			return method;
		} catch (NoSuchMethodException | SecurityException e) {
			return null;
		}
		
	}
	
	public void executeTest(Object source, String methodName, Object expectedResult) {
		
		Method m = getMethod(source.getClass(), methodName);
		
		if(m == null) {
			assertTrue(String.format(messageNoMethod, methodName), false);
		} else {
			try {
				Object result = m.invoke(source);
				assertTrue(String.format(messageFormat, methodName, expectedResult, result), result.equals(expectedResult));
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				e.printStackTrace();
				System.err.println(String.format(messageException, methodName, e.getMessage()));
			}
		}
	}
	
}
