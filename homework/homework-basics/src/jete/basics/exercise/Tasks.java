package jete.basics.exercise;

public class Tasks {
	
	/**
	 * Декларирайте няколко променливи, 
	 * като изберете за всяка една най-подходящия от типовете byte, short, int и long, 
	 * за да представят следните стойности: 52132; -111; 48259321; 98; -10011.
	 */
	public boolean declareVariables() {
		
		// write your code here
		
		
		// your code ends here
		
		return true;
	}
	
	/**
	 * Инициализирайте променлива от тип int със стойност 257 в шестнадесетичен формат 
	 * (257 е 101 в бройна система с база 16).
	 * Упътване : Числата в 16 формат започват с 0x. Пример: 0x19 = 25
	 */
	public int initializeIntVariable() {
		
		// write your code here
		
		int value = 257; // Change value here
		
		// your code ends here
		
		return value;
	}
	
	/**
	 * Създайте 3 различни променливи от тип int, long, double и ги принтирайте
	 */
	public boolean printThreeVariables() {
		
		System.out.println("Задача 3: Начало");
		
		// write your code here
		
		
		// your code ends here
		
		System.out.println("Задача 3: Край");
		
		return true;
	}
	
	/**
	 * напишете числовата репрезентация на символа 'А'.
	 */
	public int representCharAsNumber() {
		
		// write your code here
		
		int numberRepresentation = 22; // change the number here...
		
		// your code ends here
		
		return numberRepresentation;
	}
	
	/**
	 * напишете символната репрезентация на числото 65
	 */
	public char representNumberAsChar() {
		
		// write your code here
		
		char charRepresentation = 'A'; // change the literal here...
		
		// your code ends here
		
		return charRepresentation;
	}
	
	/**
	 * Дефинирайте променлива 'a' от тип int с начална стойност 100.
	 * Дефинирайте друга променлива 'b' която взима стойността на 'a' умножена * 2
	 * Дефинирайте променлива 'res' която събира 'a' и 'b' и сбора се умножава *3
	 */
	public int calculateIntResult() {
		
		// write your code here
		
		
		int res = 4343; // change that and write the previous lines of code.
		
		// your code ends here
		
		return res;
	}
	
	/**
	 * напишете израз който намира целочисления остатък при деление на числата 1234 и 563
	 */
	public int findТheReminder() {
		
		// write your code here
		
		int mainNumber = 1234;
		int devider = 563;
		
		int reminder = 123232; // change that !
		
		// your code ends here
		
		return reminder;
	}
	
	
}
