package jete.basics.solution;

public class TasksSolution {

	/**
	 * Декларирайте няколко променливи, 
	 * като изберете за всяка една най-подходящия от типовете byte, short, int и long, 
	 * за да представят следните стойности: 52132; -111; 48259321; 98; -10011.
	 */
	public boolean declareVariables() {
		
		// ----write your code here-----
		
		// int is 2 ^ 32
		int bigNumber = 52132;
		
		// long is 2 ^ 64
		long reallyBigNumber = 4825932l;
		
		// byte is 2 ^ 7 - 127 is the limit
		byte smallNumber = 98;
		byte smallNegativeNumber = -111;
		
		// short is 2 ^ 16 - 65532 is the limit
		short mediumSizeNumber = -10011;
		
		// ----your code ends here----
		
		return true;
		
	}
	
	/**
	 * Инициализирайте променлива от тип int със стойност 256 в шестнадесетичен формат 
	 * (257 е 101 в бройна система с база 16).
	 */
	public int initializeIntVariable() {
		
		// write your code here
		
		int value = 0x101;
		
		// your code ends here
		
		return value;
	}

	/**
	 * Създайте 3 различни променливи от тип int, long, double и ги принтирайте
	 */
	public boolean printThreeVariables() {
		
		// write your code here
		
		int intVar = 100;
		long longVar = 1000l;
		double doubleVal = 5001d;
				
		System.out.println(intVar);
		System.out.print(longVar);
		System.out.print(doubleVal);
		
		// your code ends here
		
		return true;
	}
	
	/**
	 * напишете числовата репрезентация на символа 'C'.
	 */
	public int representCharAsNumber() {
		
		// write your code here
		
		int numberRepresentation = 67; 
		
		// your code ends here
		
		return numberRepresentation;
	}
	
	/**
	 * напишете символната репрезентация на числото 69
	 */
	public char representNumberAsChar() {
		
		// write your code here
		
		char charRepresentation = 'E'; 
		
		// your code ends here
		
		return charRepresentation;
	}
	
	
	/**
	 * Дефинирайте променлива 'a' от тип int с начална стойност 100.
	 * Дефинирайте друга променлива 'b' която взима стойността на 'a' умножена * 2
	 * Дефинирайте променлива 'res' която събира 'a' и 'b' и сбора се умножава *3
	 */
	public int calculateIntResult() {
		
		// write your code here
		
		int a = 100;
		int b = a * 2;
		int res = (a + b) * 3;
		
		// your code ends here
		
		return res;
	}
	
	/**
	 * напишете израз който намира целочисления остатък при деление на числата 1234 и 563
	 */
	public int findТheReminder() {
		
		// write your code here
		
		int mainNumber = 1234;
		int devider = 563;
		
		int reminder = mainNumber % devider; // change that !
		
		// your code ends here
		
		return reminder;
	}
	
}
