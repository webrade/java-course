package jete.lecture6.exercise;

public class Task {

	
	/**
	 * sample method declaration could be...
	 * 
	 * public static int createArray(int n) {
	 * 
	 *    return null;
	 * }
 	 * For testing purposes, you can easily convert an 'array' to 'String' by using
	 * String stringRepresentationOfArray = Arrays.toString(yourarray);
	 * Or, of course iterate over the array and print its elements
	 * You can even write separate function to do that so you can reuse it!.
	 */
	
	/**
	 * Task 1
	 * Implement a method called 'task1' that takes int parameter 'n' as argument.
	 * create an array with size 'n' and fill the array
	 * with values from 1 to n, starting from the beginning
	 * return the sum of all elements in the array as a result
	 */
	
	
	/**
	 * Task 2
	 * Implement a method called 'task2' that takes int parameter 'n' as argument.
	 * create an array with size 'n' and fill the array with the first 'n' odd values, 
	 * with values from 1 to n, starting from the beginning
	 * e.g. 
	 * arr[0] = 1, 
	 * arr[1] = 3, 
	 * arr[2] = 5 
	 * etc.
	 * return the sum of all elements in the array as a result
	 */
	
	/**
	 * Task 3
	 * Implement a method 'task3' that takes 2 int parameters called 'arrayLength' and 'addition'.
	 * create an array with size 'arrayLength' and fill the array as follows: 
	 * arr[0] = 1 + addition * 1, 
	 * arr[1] = 2 + addition * 2, 
	 * arr[2] = 3 + addition * 3 
	 * ... etc.
	 * return the sum of all elements in the array as a result
	 */
	
	/**
	 * Task 4
	 * Implement a method 'task4' that takes 2 int parameters called 'arrayLength' and 'addition'.
	 * create an array with size 'arrayLength' and fill the array as follows: 
	 * arr[0] = 1 + addition * 1, 
	 * arr[1] = 2 + addition * 2 - arr[0], 
	 * arr[2] = 3 + addition * 3 - arr[1] 
	 * ... etc.
	 * return the sum of all elements in the array as a result
	 */
}
