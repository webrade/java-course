package homework.datastructures.solution;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Solution {
	
	
	public static int arrayListSum(List<Integer> source) {
		
		int sum = 0;
		
		for(int i : source) {
			sum += i;
		}
		
		return sum;
		
	}
	
	public static Map<Integer,Integer> arrayListComplexSum(List<Integer> source) {
		
		Map<Integer,Integer> result = new HashMap<Integer,Integer>();
		
		int key = 0;
		int value = 0;
		
		for (int i = 0; i < source.size(); i++) {
			
			if(i % 2 == 0) {
				key += source.get(i);
			} else {
				value += source.get(i);
			}
		}
		
		result.put(key, value);
		
		return result;
	}
	
	public static Map<Integer,Integer> transformList(List<Integer> source) {
		
		Map<Integer,Integer> result = new HashMap<Integer,Integer>();
		
		for (int i = 0; i < source.size(); i++) {
			result.put(i, source.get(i));
		}
		
		return result;
	}
	
	public static boolean assertListItemsLength(List<String> source) {
		
		int oddIndexTotalLength = 0;
		int evenIndexTotalLength = 0;
		
		for (int i = 0; i < source.size(); i++) {
			if(i % 2 != 0) {
				oddIndexTotalLength += source.get(i).length();
			} else {
				evenIndexTotalLength += source.get(i).length();
			}
		}
		
		return evenIndexTotalLength == oddIndexTotalLength;
	}
	
	public static Map<Integer,Integer> sumGroupedKeys(Map<String, Integer> source) {
		
		Map<Integer,Integer> result = new HashMap<Integer,Integer>();
		
		// iterate over all keys in the map
		for(String key : source.keySet()) {
			
			// get the current key length
			int currentKeyLength = key.length();
			
			// if we already have that length  as a 'key' in the result map,
			// update its value with the value for that 'key' in the source map.
			if(result.containsKey(currentKeyLength)) { 
				result.put(currentKeyLength, source.get(key) + result.get(currentKeyLength));
			} else {
				// if we didnt add previously word with that length just set the current one.
				result.put(currentKeyLength, source.get(key));
			}
		}
		
		return result;
		
	}
	
}
