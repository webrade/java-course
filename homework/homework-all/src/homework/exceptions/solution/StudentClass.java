package homework.exceptions.solution;

public class StudentClass {

	private String name;
	private int capacity;
	private String speciality;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public String getSpeciality() {
		return speciality;
	}

	public void setSpeciality(String speciality) {
		this.speciality = speciality;
	}

	@Override
	public String toString() {
		return "StudentClass [name=" + name + ", capacity=" + capacity + ", speciality=" + speciality + "]";
	}

	
}
