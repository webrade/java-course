package jete.solution;

public class Fruit {
	
	/**
	 * Define private fields.
	 * We will expose these via getter methods
	 */
	private String name = "";
	private String color = "";
	private String shape = "";
	
	/**
	 * Empty Constructor. If we create object with it, the fields will be initialised on their own - 9-11 line
	 */
	public Fruit() {}
	
	/**
	 * Specific constructor.
	 * This way, we initialise the object state properly and we won't miss data
	 */
	public Fruit(String name, String color, String shape) {
		super();
		this.name = name;
		this.color = color;
		this.shape = shape;
	}
	
	/**
	 * You can use String format method with placeholder s,
	 * or you can just concatenate strings
	 */
	public String describeThisFruit() {
		return String.format("This Fruit is %s, %s %s", getColor(), getShape(), getName());
	}
	
	/**
	 * Getter / Setter for Name field
	 */
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Getter / Setter for Color field
	 */
	public String getColor() {
		return color;
	}
	
	public void setColor(String color) {
		this.color = color;
	}
	
	/**
	 * Getter / Setter for Shape field
	 */
	public String getShape() {
		return shape;
	}
	
	public void setShape(String shape) {
		this.shape = shape;
	}

	
	
}
