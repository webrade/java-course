
public class Test {

	public static void main(String[] args) {
			
//		// int representaciqta na charB e 98
//		// kakto znaete int reprezentaciqta e posledovatelna...
//		// angliiskata azbuka ima 26 bukvi
//		char charA = 'a';
//		char charB = 'b';
//		char charZ = 'z';
//		
//		int intConstant = 10;
//		
//		int tempCharResult = charA + charB - charZ;
//		int tempIntResult = (++intConstant)  + (intConstant--);
//		
//		tempIntResult = tempIntResult + intConstant;
//		
//		int finalResult = tempCharResult + tempIntResult;
//		
//		System.out.printf("Final result is : %d", finalResult);
		
		// int representaciqta na charA e 97
		// kakto znaete int reprezentaciqta e posledovatelna...
		// angliiskata azbuka ima 26 bukvi
		char charA = 'a';
		
		// alphabet
		String sourceString = "abcdefghijklmnopqrstuvwxyz";
		
		// result
		int calculated = 0;
		
		// podkazka:
		// za da smetnete sumata na 1 + 2 + 3 + 4 + .... + n, izpolzvaite formulata n * (n + 1) / 2
		// primer: 1 + 2 + 3 + 4 + 5 + ...... + 10  = 10 * 11 / 2 = 55 !!
		
		for(int index = 0 ; index < sourceString.length() ; index = index + 1) {
			calculated = calculated + sourceString.charAt(index) + 1;
			calculated = calculated - charA;
		}
		
		System.out.println(calculated);
		
		
		
		
	}

}
