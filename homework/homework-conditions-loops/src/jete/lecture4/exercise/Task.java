package jete.lecture4.exercise;

/**
 * This is sample class with all the tasks for the current lecture.
 * Please go through all methods defined in this file and try to
 * solve the task
 * 
 * @author radko
 *
 */
public class Task {

	/**
	 * Write a program, that finds the greatest of 5 numbers;
	 * Use nested if / else or just if / else multiple times (this is better!)
	 */
	public int findGreatestNumber() {
		
		int num1 = 100;
		int num2 = 300;
		int num3 = 500;
		int num4 = 50;
		int num5 = 350;
		
		// TODO
		
		int result = num1; // assign the greatest value to that variable
		
		return result;
	}
	
	/**
	 * Write a program that shows the sign (+ or -) of the product(multiplication,  *) of three real numbers 
	 * WITHOUT calculating it. 
	 * Hint: Use sequence of if statements.
	 */
	public char findTheProductNumberSign() {
		
		float num1 = -102.20f;
		float num2 = -45.23f;
		float num3 = 812f;
		
		// in order to find the sign of the product of odd(uneven) count of numbers (in our case 3),
		// we need to make sure the negative numbers are even count;
		
		char result = '!'; // TODO
		
		return result;
		
	}
	
	/**
	 * Setup integer var n to be 100.
	 * Multiply it by 4 and divide by 8. Assign the result to another int variable called calculated, and check if it is even number and if it can be divided by 15 with no remainder
	 */
	public boolean calculationAndLogicalOperations() {
		
		int n = 0; //TODO
		
		int calculated = 0; // TODO
		
		boolean result = (calculated  == 100); // TODO
		
		return result;
		
	}
	
	/**
	 * Setup integer var n to be 1000.
	 * Write program that finds the sum of all numbers between 1 and n which can be divided by 4 and 5 at the same time.
	 * Use loop !
	 */
	public int findSum1() {
		
		int n = 1;
		
		int sum = 0;
		
		// TODO
		
		return sum;
		
	}
	
	/**
	 * Setup integer var 'n' variable to be 100.
	 * Setup string 'result' variable which is empty string at the beginning;
	 * for every number between 1 and 'n' check the following:
	 * If the number is divisible by 3 , attach to  'result' var  the string literal "Foo".
	 * If the number is divisible by 5 , attach to  'result' var  the string literal "Bar".
	 * If the number is divisible by 5 AND by 3 at the same time, attach to  'result' var the string literal "Both".
	 */
	public String easyOne() {
		
		int n = 1;
		
		String result = "test";
		
		// TODO
		
		return result;
		
	}
	
}
