package jete.lecture4.solution;

public class TaskSolution {
	
	/**
	 * Write a program, that finds the greatest of 5 numbers;
	 * Use nested if / else or just if / else multiple times
	 */
	public int findGreatestNumber() {
		
		int num1 = 100;
		int num2 = 300;
		int num3 = 500;
		int num4 = 50;
		int num5 = 350;
		
		// create another variable that will serve as a temporary holder of the current max,
		// while we are comparing it with every other number!!!
		int currentMax = 0;
		
		if(num1 > num2) {
			currentMax = num1;
		} else {
			currentMax = num2;
		}
		
		if(num3 > currentMax) {
			currentMax = num3;
		}
		
		if(num4 > currentMax) {
			currentMax = num4;
		}
		
		if(num5 > currentMax) {
			currentMax = num5;
		}
		
		int result = currentMax; // assign the greatest value to that variable
		
		return result;
	}
	
	/**
	 * Write a program that shows the sign (+ or -) of the product(multiplication,  *) of three real numbers 
	 * WITHOUT calculating it. 
	 * Hint: Use sequence of if statements.
	 */
	public char findTheProductNumberSign() {
		
		float num1 = -102.20f;
		float num2 = 45.23f;
		float num3 = -812f;
		
		// in order to find the sign of the product of odd(uneven) count of numbers (in our case 3),
		// we need to make sure the negative numbers are even count;
		
		// begin counting the negative signs
		int negativeSignsCount = 0;
		
		if(num1 < 0) {
			negativeSignsCount++; // increment the count of the negative signs number
		}
		
		if(num2 < 0) {
			negativeSignsCount++; // increment the count of the negative signs number
		}
		
		if(num3 < 0) {
			negativeSignsCount++; // increment the count of the negative signs number
		}
		
		char result = '!';
		
		// event number of negative numbers (-1 * -1 = 1) !!!!
		if(negativeSignsCount % 2 == 0) {
			result = '+';
		} else {
			result = '-';
		}
		
		return result;
		
	}
	
	/**
	 * Setup integer var n to be 100.
	 * Multiply it by 4 and divide by 8. Assing the result to another int variable called calculated, and check if it is even number and if it can be divided by 15 with no remainder
	 */
	public boolean calculationAndLogicalOperations() {
		
		int n = 100;
		
		int calculated =  n * 4 / 8 ;
		
		// first condition check for even number, second for 
		boolean result = (calculated % 2 == 0 && calculated % 15 == 0); 
		
		return result;
		
	}
	
	/**
	 * Setup integer var n to be 1000.
	 * Write program that finds the sum of all numbers between 1 and n which can be divided by 4 and 5 at the same time.
	 * Use loop !
	 */
	public int findSum1() {
		
		int n = 1000;
		
		int sum = 0;
		
		// solution 1
		//Why this is not good ?
		
//		for(int i = 1 ; i < n ; i++) {
//			
//			if(i % 4 == 0 && i % 5 == 0) {
//				sum += i;
//			}
//			
//		}
		
		// Way Way better !!!
		for(int i = 0 ; i < n ; i = i + (4 * 5)) {
			sum += i;
		}
		
		return sum;
		
	}
	
	/**
	 * Setup integer var 'n' variable to be 100.
	 * Setup string 'result' variable which is empty string at the beginning;
	 * for every number between 1 and 'n' check the following:
	 * If the number is divisible by 3 , attach to  'result' var  the string literal "Foo".
	 * If the number is divisible by 5 , attach to  'result' var  the string literal "Bar".
	 * If the number is divisible by 5 AND by 3 at the same time, attach to  'result' var the string literal "Both".
	 */
	public String easyOne() {
		
		int n = 100;
		
		String result = "";
		
		for(int i = 1 ; i < 100 ; i ++) {
			
			if(i % 15 == 0) {
				result = result + "Both";
				continue;
			}
			
			if(i % 3 == 0) {
				result = result + "Foo";
				//no continue why ???
			}
			
			if(i % 5 == 0) {
				result = result + "Bar";
			}
			
		}
		
		return result;
		
	}
	
}
